/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_RUNTIME_IO_PORT_H_
#define CORE_INCLUDE_EQUINOX_RUNTIME_IO_PORT_H_

#include <equinox/core/core.h>

#include <string>

namespace eqnx
{

/**
 * @ingroup runtime
 *
 * This class represents an IO port of a kernel. Each kernel may have an
 * arbitrary number of input or output ports that can retrieve or produce
 * data.
 */
class io_port
{
public:
  /**
   * The direction of the IO port
   */
  typedef enum
  {
    IO_PORT_DIR_INPUT = 0, //!< The port is an input port
    IO_PORT_DIR_OUTPUT    //!< The port is an output port
  } io_port_dir_t;

  /* We do not need someone to be able to create this class directly */
  virtual
  ~io_port () = 0;

  std::string
  name ();

  size_t
  id ();

  io_port_dir_t
  get_direction ();

  bool
  is_input ();

  bool
  is_output ();

  bool
  is_async ();

  bool
  is_ready ();

  friend std::ostream&
  operator<< (std::ostream& out, const io_port& p);

protected:
  io_port (const std::string& name, io_port_dir_t dir, bool async = false);

  bool d_is_ready;

private:
  const std::string d_name;
  const io_port_dir_t d_dir;
  const bool d_async;
};

} /* namespace eqnx */

#endif /* CORE_INCLUDE_EQUINOX_RUNTIME_IO_PORT_H_ */
