/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_RUNTIME_INPUT_PORT_H_
#define CORE_INCLUDE_EQUINOX_RUNTIME_INPUT_PORT_H_

#include <equinox/core/core.h>
#include <equinox/runtime/io_port.h>
#include <equinox/core/mm/msg_queue.h>
#include <equinox/core/mm/msg_queue_consumer.h>

namespace eqnx
{

/**
 * @ingroup runtime
 */
class input_port : public virtual io_port
{
public:
  typedef std::shared_ptr<input_port> sptr;

  static sptr
  make_shared (const std::string& name);

  void
  setup (core::msg_queue::sptr msgq);

  msg::sptr
  read ();

  bool
  empty();

  template<typename _Rep>
    msg::sptr
    read (const std::chrono::duration<_Rep>& timeout);

protected:
  input_port (const std::string& name);
private:
  core::msg_queue_consumer::uptr d_msg_consumer;
};

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_RUNTIME_INPUT_PORT_H_ */
