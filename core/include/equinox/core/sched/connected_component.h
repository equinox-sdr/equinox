/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017,2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTED_COMPONENT_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTED_COMPONENT_H_

#include <equinox/runtime/kernel.h>
#include <equinox/core/sched/connection.h>
#include <equinox/core/sched/connection_graph_node.h>

#include <deque>
#include <map>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * A connected component is a subgraph that is not connected with the
 * supergraph. Therefore, processing kernels operating on the same
 * connected component, have no relationship or dependency on the rest of
 * the kernels of the supergraph. Equinox assigns handles kernels of
 * different connected components as completely different processing
 * entities, thus they assigned to different workers.
 *
 * This class is a convenient interface to allow easy and fast access to
 * the kernels of the same connected component.
 */
class connected_component
{
public:
  typedef std::shared_ptr<connected_component> sptr;

  static sptr
  make_shared ();

  void
  add_kernel (kernel::sptr k, connection_graph_node::sptr knode);

  void
  analyze ();

  bool
  is_analyzed ();

  const std::deque<kernel::sptr>&
  get_kernels ();

  size_t
  size ();

private:
  connected_component ();

  void
  topological_sort ();

  bool d_analyzed;

  std::deque<kernel::sptr> d_sources;
  std::deque<kernel::sptr> d_sinks;

  /**
   * List of the kernels in topological order.
   * Sources are in front whereas sinks on the back
   */
  std::deque<kernel::sptr> d_top_order;
  std::map<kernel::sptr, connection_graph_node::sptr> d_nodes_map;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTED_COMPONENT_H_ */
