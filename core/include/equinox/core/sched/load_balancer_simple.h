/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_SIMPLE_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_SIMPLE_H_

#include <equinox/core/sched/load_balancer.h>

namespace eqnx
{

namespace core
{
/**
 * @ingroup core
 *
 * @brief This class implements a naive and simple load balancing scheme
 * that is fast and may work well in non-complex graphs. It can be used
 * in cases where other techniques cannot applied or a very simple and
 * easy to follow resources distribution is required (e.g debugging).
 *
 * It tries to split the kernels of each connected component evenly
 * across the available workers.
 */
class load_balancer_simple : public virtual load_balancer
{
public:
  load_balancer_simple (connection_graph::sptr graph, size_t workers_num = 1);

  bool
  analyze ();

  void
  reset ();

  std::deque<kernel::sptr>
  get_worker_assignement (size_t index);

  bool
  find_kernel(const size_t index, const kernel::sptr k);

private:
  /**
   * This margin allows some kernels to be merged in larger groups
   * to avoid excessive fragmentation.
   */
  const float d_split_margin;
  /**
   * This vector holds the kernels assignment on every worker.
   * The list of the kernels is in topological order
   */
  std::vector<std::shared_ptr<std::deque<kernel::sptr>>> d_workers_assignement;

  void
  balance_connected_components ();

  size_t
  balance_connected_component (size_t index, size_t available_workers,
                               size_t kernels_per_cc);
};
}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_SIMPLE_H_ */
