/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_OUTER_SCHED_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_OUTER_SCHED_H_

#include <equinox/core/sched/inner_sched.h>
#include <equinox/core/sched/connection.h>
#include <equinox/runtime/exception.h>
#include <equinox/core/sched/connection_graph.h>
#include <equinox/core/sched/load_balancer.h>
#include <equinox/core/threading/worker.h>
#include <equinox/core/mm/mm.h>

#include <deque>
#include <vector>
#include <chrono>
#include <thread>
#include <type_traits>
#include <algorithm>

namespace eqnx
{

namespace core
{

/**
 * @ingroup sched
 *
 * The outer scheduler is responsible for the creation and management of
 * the worker threads. In each worker thread, a separate inner scheduler is
 * assigned among with the corresponding kernels.
 *
 * During the process termination or after the stop() method invocation
 * the outer scheduler informs the inner schedulers to also stop and exit.
 *
 * In addition, outer scheduler provides a watchdog (by default disabled)
 * that tracks the progress of the worker threads. If no activity has been
 * observed for a specific amount of time, the outer scheduler forces the
 * termination of the worker threads and restarts them. To avoid excess CPU
 * utilization, the necessary progress checks are performed with granularity
 * up to one second. If a more fine-grained watchdog update interval is
 * requested, a runtime error is raised.
 */
template <typename T1, typename T2>
class outer_sched
{
public:
  static_assert (std::is_base_of<eqnx::core::load_balancer, T1>::value,
        "T1 must extend eqnx::core::load_balancer");

  outer_sched (connection_graph::sptr g);
  outer_sched (size_t n_workers, connection_graph::sptr g);

  /* Outer scheduler can NOT be copied */
  outer_sched (const outer_sched& c) = delete;

  ~outer_sched ();

  void
  start ();

  void
  stop ();

  void
  join ();

  void
  enable_watchdog (bool enable);

  void
  reset_watchdog ();

  template<typename _Rep>
    void
    watchdog_set_timeout (const std::chrono::duration<_Rep>& timeout)
  {
    int64_t t =
        std::chrono::duration_cast<std::chrono::seconds> (timeout).count ();
    if (t > 0) {
      d_watchdog_timeout_secs = t;
    }
    else {
      throw std::runtime_error ("Invalid watchdog timeout value");
    }
  }

private:
  const size_t                  d_max_n_workers;
  bool                          d_stop;
  bool                          d_watchdog_on;
  uint64_t                      d_watchdog_timeout_secs;
  size_t                        d_n_workers;
  std::deque<worker<T2>>        d_workers;
  connection_graph::sptr        d_g;
  T1                            d_load_balancer;

  void
  _start ();

  void
  setup_connections();

  void
  setup_kernel_connections(kernel::sptr k, size_t worker_idx);

};

/**
 *
 * @return the number of the available cores of the host. If this number
 * cannot be determined this function returns always 1.
 */
static inline size_t
get_host_cores_num ()
{
  size_t ret = std::thread::hardware_concurrency ();
  if (ret == 0) {
    return 1;
  }
  return ret;
}

template<typename T1, typename T2>
outer_sched<T1, T2>::outer_sched (connection_graph::sptr g) :
        outer_sched<T1, T2> (get_host_cores_num (), g)
{
}

template<typename T1, typename T2>
  outer_sched<T1, T2>::outer_sched (size_t n_workers, connection_graph::sptr g) :
          d_max_n_workers (n_workers),
          d_stop (false),
          d_watchdog_on (false),
          d_watchdog_timeout_secs (1), /* TODO: Get it from config file? */
          d_n_workers (0),
          d_g(g),
          d_load_balancer (g, n_workers)
  {
    /* Perform graph analysis and connections configuration */
    d_load_balancer.analyze ();
    d_n_workers = d_load_balancer.actual_workers_num ();
    for (size_t i = 0; i < d_n_workers; i++) {
      d_workers.emplace_back ("worker" + std::to_string (i), i);
    }
    for (size_t i = 0; i < d_n_workers; i++) {
      d_workers[i].set_kernels (d_load_balancer.get_worker_assignement (i));
    }

    /* Create memory pools, message queues and setup the connections */
    setup_connections();
  }

template<typename T1, typename T2>
  outer_sched<T1, T2>::~outer_sched ()
  {
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::setup_kernel_connections (kernel::sptr k,
                                                   size_t worker_idx)
  {
    size_t noutputs = k->output_num();
    connection_graph_node::sptr node = (*d_g)[k];
    std::vector<std::string> output_names = node->get_output_ports();

    if(!node) {
      throw exception(exception::INVALID_GRAPH);
    }
    if(noutputs != output_names.size()) {
      throw exception(exception::INVALID_GRAPH);
    }

    /*
     * For every output connection of this kernel, check if the corresponding
     * destination input port belongs to the same worker.
     *
     * If at least one output port is connected with a kernel of another
     * worker, the thread safe pool and message queue should be assigned to this
     * output port
     */
    for(const std::string& i : output_names) {
      std::deque<connection::sptr> conns = node->get_ouput_port_connections(i);
      bool same_worker = true;
      for(connection::sptr c : conns) {
        if(!d_load_balancer.find_kernel(worker_idx, c->dst_kernel ())) {
          same_worker = false;
          break;
        }
      }
      mem_pool::sptr mp;
      msg_queue::sptr mq;
      if(same_worker) {
        /*
         * Create a memory pool and the message queue suitable for in-worker
         * communication
         */
        /* FIXME! messages sizes should be retrieved by the port configuration */
        mp = mem_pool_simple::make_shared (4096, 1024);
        mq = msg_queue_simple::make_shared ("mpla", 4096, conns.size ());
      }
      else{
        /* FIXME! messages sizes should be retrieved by the port configuration */
        /*
         * Create a memory pool and the message queue suitable for communication
         * between kernels belonging to different workers
         */
        mp = mem_pool_r::make_shared (4096, 1024);
        mq = msg_queue_r::make_shared ("mpla", 4096, conns.size ());
      }

      /*
       * Assign the message pool and the message queue at the corresponding
       * output message port
       */
      k->setup_output_port(i, mp, mq);

      /* Now assign the message queue at the registered message readers */
      for (connection::sptr c : conns) {
        const std::string& pname = c->dst_port()->name();
        std::cout << "Configuring " << pname << std::endl;
        kernel::sptr dst_kernel = c->dst_kernel();
        dst_kernel->setup_input_port(pname, mq);
      }
    }
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::setup_connections ()
  {
    /*
     * For every kernel in the worker prepare the message pools and their
     * corresponding message queues. This should be done prior any
     * actual connection establishment.
     */
    for (size_t i = 0; i < d_n_workers; i++) {
      std::deque<kernel::sptr> kernels =
          d_load_balancer.get_worker_assignement (i);
      for (kernel::sptr k : kernels) {
        setup_kernel_connections (k, i);
      }
    }
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::start ()
  {
    for (size_t i = 0; i < d_n_workers; i++) {
      d_workers[i].start();
    }
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::join ()
  {
    for (size_t i = 0; i < d_n_workers; i++) {
      d_workers[i].join ();
    }
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::stop ()
  {
    d_stop = true;
    for (size_t i = 0; i < d_n_workers; i++) {
      d_workers[i].stop();
    }
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::enable_watchdog (bool enable)
  {
    d_watchdog_on = enable;
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::reset_watchdog ()
  {
  }

template<typename T1, typename T2>
  void
  outer_sched<T1, T2>::_start ()
  {
    d_stop = false;
  }

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_OUTER_SCHED_H_ */
