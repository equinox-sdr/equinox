/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_GRAPH_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_GRAPH_H_

#include <equinox/core/core.h>
#include <equinox/core/sched/connection_graph_node.h>
#include <equinox/runtime/kernel.h>
#include <map>
#include <iterator>

#include "connected_component.h"

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * The connection graph is a directional graph that describes the
 * connections between the processing kernels.
 *
 * The graph uses the std::map class having as keys the shared pointers of
 * each kernel.
 */
class connection_graph
{
public:
  typedef std::shared_ptr<connection_graph> sptr;

  static sptr
  make_shared ();

  size_t
  size ();

  bool
  is_analyzed ();

  void
  add_connection (connection::sptr c);

  void
  analyze ();

  friend std::ostream&
  operator<< (std::ostream& out, const connection_graph& g);

  connection_graph_node::sptr
  operator[] (kernel::sptr k);

  size_t
  get_connected_components_num ();

  const std::deque<kernel::sptr>&
  get_connected_component_kernels (size_t index);

  size_t
  get_connected_component_size (size_t index);

protected:
  connection_graph ();

private:
  bool d_analyzed;

  /**
   * Total number of edges
   */
  size_t d_edge_num;

  std::map<kernel::sptr, connection_graph_node::sptr> d_nodes;
  std::deque<kernel::sptr> d_sources;
  std::deque<kernel::sptr> d_sinks;
  std::deque<kernel::sptr> d_top_order;
  std::deque<connected_component::sptr> d_connected_components;

  void
  invalidate ();

  void
  dfs (kernel::sptr node);

  void
  find_connected_components ();

};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_GRAPH_H_ */
