/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_SPECTRAL_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_SPECTRAL_H_

#include <equinox/core/sched/load_balancer.h>

namespace eqnx
{

namespace core
{
/**
 * @ingroup core
 *
 * @brief This class provide a more sophisticated load balancing method
 * comparing to the eqnx::core::load_balancer_simple. It is based on the
 * eigenvalue and eigenvector spectral method for graph partitioning based
 * on the Fiedler's vector \cite fiedler1975property. The goal of this method
 * is to actually minimize the number of edges between the sub-graphs.
 * This can have a significant positive impact in communication intensive
 * kernels. Keeping the heavy communication between kernels in the same
 * thread will decrease the synchronization overhead, thus increasing the
 * performance. It requires the Eigen3 library for computing efficiently the
 * required eignevalues and the eigenvectors.
 *
 * Note that the balancing is not optimal in a global sense.
 */
class load_balancer_spectral : public virtual load_balancer
{
public:
  load_balancer_spectral (connection_graph::sptr graph, size_t workers_num = 1,
                          size_t min_partition_size = 2);

  bool
  analyze ();

  void
  reset ();

  std::deque<kernel::sptr>
  get_worker_assignement (size_t index);

  bool
  find_kernel(const size_t index, const kernel::sptr k);

private:
  const size_t d_min_partition_size;
  typedef std::vector<std::shared_ptr<std::deque<kernel::sptr> > > partition_result_t;

  /**
   * This vector holds the kernels assignment on every worker.
   * The list of the kernels is in topological order
   */
  std::vector<std::shared_ptr<std::deque<kernel::sptr>>> d_workers_assignement;

  void
  balance_connected_components ();

  void
  spectral_graph_partition_recursive (
      const std::shared_ptr<std::deque<kernel::sptr>> g, size_t k);

  partition_result_t
  spectral_graph_partition (const std::shared_ptr<std::deque<kernel::sptr>> g);
};
}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_SPECTRAL_H_ */
