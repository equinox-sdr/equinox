/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_INNER_SCHED_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_INNER_SCHED_H_

#include <equinox/core/core.h>
#include <equinox/runtime/kernel_runnable.h>

namespace eqnx
{

namespace core
{

/**
 * @ingroup sched
 *
 * The inner scheduler is responsible for the execution of the kernels that
 * belong on the same worker thread.
 *
 * This is a base class covering some methods that are common across all
 * available inner scheduler implementations.
 *
 * All the available inner scheduler should implement the run() method,
 * which is responsible for the proper execution of the assigned kernels.
 */
class inner_sched
{
public:
  typedef std::shared_ptr<inner_sched> sptr;

  /* We do not need someone to be able to create this class directly */
  virtual
  ~inner_sched () = 0;

  /**
   * The main execution method. It is responsible for the proper workload
   * distribution upon all the assigned kernels.
   * Normally this method is executed in a separate thread and has the
   * life-span of the process itself.
   * The exact behavior of the method is defined by the derived class
   * implementation.
   *
   * The method should stop after the call of the stop() method or on any
   * error.
   */
  virtual void
  run () = 0;

  /**
   * ALL inner scheduler derived classes should provide a dummy variation
   * of the run() method so inner schedulers can be tested using
   * the CPPUnit testing framework.
   *
   * This method should also conform on the stop() method, exiting ASAP after
   * its call.
   */
  virtual void
  dry_run () = 0;

  /**
   * Inserts a kernel to the inner scheduler. If the setup of the scheduler
   * has been completed, no new kernel can be inserted. Thus, this method
   * will raise a runtime exception.
   *
   * @param kernel the kernel that will be inserted to the scheduler
   */
  virtual void
  insert_kernel (kernel_runnable::sptr kernel) = 0;

  /**
   *
   * @return the name of the inner scheduler
   */
  std::string
  name ();

  /**
   *
   * @return true if the scheduler has been successfully configured and
   * is ready to execute the run() method, false otherwise
   */
  bool
  is_ready ();

  /**
   * Starts the execution of the scheduler. run() and dry_run() methods
   * should block until this method is called.
   */
  virtual void
  start () = 0;

  /**
   * After the call of this method the run() method should initialize
   * all the necessary procedures to stop its execution and exit.
   */
  virtual void
  stop ();

  /**
   * Sets the scheduler to a state that is ready to be executed.
   * After this method invocation, no core modifications (e.g adding a
   * kernel) is allowed. Any attempt, will raise an exception.
   */
  virtual void
  set_ready ();

  bool
  is_running ();

  friend std::ostream&
  operator<< (std::ostream& out, const inner_sched& y);

protected:
  inner_sched (const std::string& name);

  size_t d_n_kernels;
  bool d_is_ready;
  bool d_running;
  bool d_start;
  bool d_stop;
private:
  const std::string d_name;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_INNER_SCHED_H_ */
