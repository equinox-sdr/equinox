/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_H_

#include <equinox/core/core.h>
#include <equinox/runtime/kernel.h>
#include <equinox/runtime/input_port.h>
#include <equinox/runtime/output_port.h>

namespace eqnx
{

namespace core
{
/**
 * @ingroup core
 *
 * A connection is a binding between an output port of the source kernel
 * and the corresponding input port of the destination kernel.
 */
class connection
{
public:
  typedef std::shared_ptr<connection> sptr;

  static sptr
  make_shared (const connection_anchor& src, const connection_anchor& dst);

  kernel::sptr
  src_kernel ();

  kernel::sptr
  dst_kernel ();

  output_port::sptr
  src_port ();

  input_port::sptr
  dst_port ();

  float
  weight ();

  void
  set_weight (float w);

  bool
  is_processed ();

  void
  set_processed (bool processed);

protected:
  connection (const connection_anchor& src, const connection_anchor& dst);
private:
  kernel::sptr                  d_src;
  kernel::sptr                  d_dst;
  output_port::sptr             d_src_port;
  input_port::sptr              d_dst_port;
  float                         d_weight;
  bool                          d_processed;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_H_ */
