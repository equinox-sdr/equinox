/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_CIRC_BUFFER_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_CIRC_BUFFER_H_

#include <equinox/log.h>
#include <equinox/utils.h>

#include <vector>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * @brief A fixed size circular buffer that can be used as a convenient
 * double ended FIFO. The capacity is automatically aligned to the next
 * power of 2, so the insert and extract operations can avoid costly modulus
 * operations.
 */
template<class T>
  class circ_buffer
  {
  private:
    const size_t d_n_readers;
    size_t d_capacity;
    size_t d_head;
    size_t d_tail;
    std::vector<size_t> d_tails;
    std::vector<size_t> d_ack;
    T* d_mem;
  public:
    circ_buffer (size_t n_readers = 1, size_t size = 1024);
    ~circ_buffer ();

    size_t
    get_readers_num ();
    bool
    empty ();
    bool
    empty (size_t reader);
    bool
    full ();
    void
    push (T const &elem);
    T
    pop (size_t reader);
    size_t
    capacity ();
    size_t
    cnt ();
    size_t
    cnt (size_t reader);
    size_t
    space ();

  };

/**
 * Creates a fixed size circular buffer that can hold T objects
 *
 * @param size the number of T objects that the circular buffer can hold.
 * Note that if size is not a power of 2, the constructor will try to
 * allocate memory for a number of elements equal to the next power of 2.
 *
 * Eventually the true number of elements would be the size of the allocated
 * space minus 1 element due to internal logic requirements. Users should
 * always check the available space through the space() method.
 */
template<class T>
  inline
  circ_buffer<T>::circ_buffer (size_t n_readers, size_t size) :
          d_n_readers (n_readers),
          d_head (0),
          d_tail (0),
          d_tails (n_readers, 0)
  {
    size_t actual_size;
    /* Circular buffers can be very fast when the capacity is a power of 2*/
    if (size == 0) {
      throw std::invalid_argument ("Capacity should be > 0");
    }
    /* Find the next power of two */
    actual_size = get_next_pow2 (size);
    if (actual_size != size) {
      EQNX_LOG_WARN(
          "Setting the requested capacity %zu to the next" " power-of-two %zu",
          size, actual_size);
    }
    d_capacity = actual_size;
    d_mem = new T[d_capacity];
    d_ack = std::vector<size_t> (d_capacity, 0);
  }

template<class T>
  inline
  circ_buffer<T>::~circ_buffer ()
  {
    delete[] d_mem;
  }

/**
 * @return the number of readers assigned with this circular buffer
 */
template<class T>
  inline size_t
  circ_buffer<T>::get_readers_num ()
  {
    return d_n_readers;
  }

/**
 * Checks if the circular buffer is empty
 * @return true if it is empty, false otherwise
 */
template<class T>
  inline bool
  circ_buffer<T>::empty ()
  {
    return cnt () == 0;
  }

/**
 * Checks if the circular buffer has no elements for a specific reader
 * @param reader the reader ID
 * @return true if it there is no element to dequeue, false otherwise
 */
template<class T>
  inline bool
  circ_buffer<T>::empty (size_t reader)
  {
    return cnt (reader) == 0;
  }

/**
 * Checks if the circular buffer is full
 * @return true if it is full, false otherwise
 */
template<class T>
  inline bool
  circ_buffer<T>::full ()
  {
    return space () == 0;
  }

/**
 * Push a new element
 * @param elem the new element
 */
template<class T>
  inline void
  circ_buffer<T>::push (const T& elem)
  {
    d_mem[d_head] = elem;
    d_ack[d_head] = 0;
    d_head = (d_head + 1) & (d_capacity - 1);
  }

/**
 * Pops an element from the front of circular buffer
 * @return the popped element
 */
template<class T>
  inline T
  circ_buffer<T>::pop (size_t reader)
  {
    T t;
    t = d_mem[d_tails[reader]];
    d_ack[d_tails[reader]]++;

    /*
     * If all readers consumed an element propagate the global tail so
     * the producer can continue pushing new items
     */
    if (d_ack[d_tails[reader]] == d_n_readers) {
      d_tail = (d_tail + 1) & (d_capacity - 1);
    }
    d_tails[reader] = (d_tails[reader] + 1) & (d_capacity - 1);
    return t;
  }

/**
 *
 * @return the count of holding elements of the circular buffer
 */
template<class T>
  inline size_t
  circ_buffer<T>::cnt ()
  {
    return (d_head - d_tail) & (d_capacity - 1);
  }

/**
 * Gets the number of available items in the circular buffer that have
 * not been yet dequeued from the circular buffer for a specific reader.
 * @param reader the reader ID
 * @return the count of holding elements of the circular buffer for a
 * specific reader
 */
template<class T>
  inline size_t
  circ_buffer<T>::cnt (size_t reader)
  {
    return (d_head - d_tails[reader]) & (d_capacity - 1);
  }

/**
 *
 * @return the available space in the circular buffer
 */
template<class T>
  inline size_t
  circ_buffer<T>::space ()
  {
    return (d_tail - (d_head + 1)) & (d_capacity - 1);
  }

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_CIRC_BUFFER_H_ */
