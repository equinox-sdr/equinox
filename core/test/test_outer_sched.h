/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_OUTER_SCHED_H_
#define CORE_TEST_TEST_OUTER_SCHED_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <equinox/core/core.h>
#include <equinox/core/sched/outer_sched.h>
#include <equinox/core/sched/load_balancer_spectral.h>
#include <equinox/core/sched/inner_sched_rr.h>

namespace eqnx
{

namespace core
{

class test_outer_sched : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_outer_sched);
  CPPUNIT_TEST(create_connections);
  CPPUNIT_TEST_SUITE_END();
public:

  void
  create_connections ();

  void
  stop_outer_sched();

private:
   outer_sched<load_balancer_spectral, inner_sched_rr> *d_os;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_TEST_TEST_OUTER_SCHED_H_ */
