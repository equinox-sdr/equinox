/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <thread>
#include <chrono>
#include <string.h>

#include <cppunit/TestCase.h>
#include <equinox/core/core.h>

#include <equinox/core/mm/mem_pool.h>
#include <equinox/core/mm/mem_pool_simple.h>
#include <equinox/core/mm/mem_pool_r.h>

#include "test_msg_queue.h"

#define CONSUMERS_NUM 10

namespace eqnx
{

namespace core
{
void
test_msg_queue::setUp ()
{
  d_q = msg_queue_r::make_shared ("test", 1024, CONSUMERS_NUM);
  d_mp_simple = mem_pool_simple::make_shared (2048, 1024);
  d_mp_r = mem_pool_simple::make_shared (2048, 1024);
}

void
test_msg_queue::tearDown ()
{
  d_q->clear ();
}

void
test_msg_queue::test_msg_queue_lock_sinlge_consumer ()
{
  int ret;
  size_t i;
  msg_queue::sptr q = msg_queue_r::make_shared ("mpa", 2048);

  EQNX_LOG_INFO("Testing test_msg_queue_lock_sinlge_consumer()");

  for (i = 0; i < 1000; i++) {
    msg::sptr m = d_mp_r->new_msg ();
    ret = q->push (m);
    CPPUNIT_ASSERT(ret == msg_queue::EQNX_MSGQ_NO_ERROR);
  }

  for (i = 0; i < 1000; i++) {
    msg::sptr m = q->pop (0);
    CPPUNIT_ASSERT(m != nullptr);
  }
}

void
test_msg_queue::test_msg_queue_lock_multiple_consumers ()
{
  int ret;
  size_t i;

  EQNX_LOG_INFO("Testing test_msg_queue_lock_multiple_consumers()");

  d_q->clear ();
  for (i = 0; i < 1000; i++) {
    msg::sptr m = d_mp_r->new_msg ();
    ret = d_q->push (m);
    CPPUNIT_ASSERT(ret == msg_queue::EQNX_MSGQ_NO_ERROR);
  }

  for (i = 0; i < 1000; i++) {
    for (size_t j = 0; j < CONSUMERS_NUM; j++) {
      msg::sptr m = d_q->pop (j);
      CPPUNIT_ASSERT(m != nullptr);
    }
  }
}

void
test_msg_queue::producer ()
{
  int ret;
  size_t cnt = 0;
  while (d_running) {
    try {
      msg::sptr m = d_mp_r->new_msg ();
      memcpy (m->raw_ptr (), &cnt, sizeof(size_t));
      ret = d_q->push_timedwait (m, 10000);
      if (ret == msg_queue::EQNX_MSGQ_TIMER_EXPIRED) {
        continue;
      }
      cnt++;
      CPPUNIT_ASSERT(ret == msg_queue::EQNX_MSGQ_NO_ERROR);
    }
    catch (msg_queue_exception& e) {
      break;
    }
  }
  EQNX_LOG_INFO("Produced %zu messages", cnt);
}

void
test_msg_queue::consumer (size_t reader_id)
{
  size_t key;
  size_t cnt = 0;
  while (d_running) {
    try {
      msg::sptr m;
      m = d_q->pop_timedwait (reader_id, 10000UL);
      if (!m) {
        continue;
      }
      memcpy (&key, m->raw_ptr (), sizeof(size_t));
      CPPUNIT_ASSERT(m != nullptr);
      /* Producer creates messages with monotonically increasing keys */
      CPPUNIT_ASSERT(key == cnt);
      cnt++;
    }
    catch (msg_queue_exception& e) {
      return;
    }
  }
}

void
test_msg_queue::test_msg_queue_lock_multiple_consumers_threaded ()
{
  size_t i;
  d_running = true;
  std::thread con[CONSUMERS_NUM];
  std::thread pr;

  EQNX_LOG_INFO("Testing test_msg_queue_lock_multiple_consumers_threaded()");

  for (i = 0; i < CONSUMERS_NUM; i++) {
    con[i] = std::thread (&test_msg_queue::consumer, this, i);
  }
  pr = std::thread (&test_msg_queue::producer, this);
  std::this_thread::sleep_for (std::chrono::seconds (5));
  d_running = false;
  EQNX_LOG_INFO("Inform threads to stop...");
  pr.join ();
  EQNX_LOG_INFO("Producer stopped");
  for (i = 0; i < CONSUMERS_NUM; i++) {
    con[i].join ();
    EQNX_LOG_INFO("Consumer %zu stopped", i);
  }
}

void
test_msg_queue::test_msg_queue_simple ()
{
  size_t data;
  msg_queue::sptr q = msg_queue_simple::make_shared ("simple", 1024,
  CONSUMERS_NUM,
                                                     false, false);

  EQNX_LOG_INFO("Testing test_msg_queue_simple()");

  for (size_t i = 0; i < 10000; i++) {
    msg::sptr m = d_mp_simple->new_msg ();
    memcpy (m->raw_ptr (), &i, sizeof(size_t));
    q->push (m);
    for (size_t j = 0; j < CONSUMERS_NUM; j++) {
      m = q->pop (j);
      CPPUNIT_ASSERT(m != nullptr);
      memcpy (&data, m->raw_ptr (), sizeof(size_t));
      CPPUNIT_ASSERT(i == data);
    }
  }
}

void
test_msg_queue::test_msg_queue_consumer ()
{
  size_t data;
  msg_queue_consumer::uptr consumers[CONSUMERS_NUM];
  msg_queue::sptr sq = msg_queue_simple::make_shared ("simple", 1024,
  CONSUMERS_NUM,
                                                      false, false);

  EQNX_LOG_INFO("Testing test_msg_queue_consumer()");

  for (size_t i = 0; i < CONSUMERS_NUM; i++) {
    consumers[i] = msg_queue_consumer::make ("test", i, sq);
  }

  for (size_t i = 0; i < 10000; i++) {
    msg::sptr m = d_mp_simple->new_msg ();
    memcpy (m->raw_ptr (), &i, sizeof(size_t));
    sq->push (m);
    for (size_t j = 0; j < CONSUMERS_NUM; j++) {
      m = consumers[j]->read ();
      CPPUNIT_ASSERT(m != nullptr);
      memcpy (&data, m->raw_ptr (), sizeof(size_t));
      CPPUNIT_ASSERT(i == data);
    }
  }
}

}  // namespace core

}  // namespace eqnx
