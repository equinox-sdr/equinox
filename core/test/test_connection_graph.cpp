/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_connection_graph.h"

#include <equinox/core/sched/load_balancer.h>
#include <equinox/core/sched/load_balancer_simple.h>
#include <equinox/core/sched/load_balancer_spectral.h>

#include <equinox/kernels/testing/sink.h>
#include <equinox/kernels/testing/source.h>
#include <equinox/kernels/testing/in_out.h>

namespace eqnx
{

namespace core
{
void
test_connection_graph::setUp ()
{
  d_graph = connection_graph::make_shared ();
}

void
test_connection_graph::test_connections ()
{
  testing::sink::sptr sink0 = testing::sink::make_shared ("sink0", 2);
  testing::source::sptr source0 = testing::source::make_shared ("source0");
  testing::source::sptr source1 = testing::source::make_shared ("source1");
  testing::in_out::sptr inout = testing::in_out::make_shared ("inout");
  testing::in_out::sptr not_connected = testing::in_out::make_shared (
      "not_connected");

  /* Check that initially the graph does not contain any edges */
  CPPUNIT_ASSERT(d_graph->size () == 0);

  /* Duplicates are tested from the outer scheduler. */
  d_graph->add_connection ((*source0)["out0"] >> (*sink0)["in0"]);
  d_graph->add_connection ((*source1)["out0"] >> (*inout)["in0"]);

  CPPUNIT_ASSERT(d_graph->size () == 4);
  d_graph->add_connection ((*source1)["out0"] >> (*inout)["in0"]);
  d_graph->add_connection ((*inout)["out0"] >> (*sink0)["in1"]);
  CPPUNIT_ASSERT(d_graph->size () == 4);

  d_graph->analyze ();

  /* Get and set some random weights on the kernels */
  CPPUNIT_ASSERT((*d_graph)[source0]->weight () <= 1.0f);
  CPPUNIT_ASSERT((*d_graph)[source0]->weight () > 0.9999f);
  CPPUNIT_ASSERT((*d_graph)[source1]->weight () <= 1.0f);
  CPPUNIT_ASSERT((*d_graph)[source1]->weight () > 0.9999f);
  CPPUNIT_ASSERT((*d_graph)[sink0]->weight () <= 1.0f);
  CPPUNIT_ASSERT((*d_graph)[sink0]->weight () > 0.9999f);
  CPPUNIT_ASSERT((*d_graph)[inout]->weight () <= 1.0f);
  CPPUNIT_ASSERT((*d_graph)[inout]->weight () > 0.9999f);

  /* Set an invalid weight. That should raise an exception */
  CPPUNIT_ASSERT_THROW((*d_graph)[inout]->set_weight (0.0), std::exception);
  CPPUNIT_ASSERT_THROW((*d_graph)[inout]->set_weight (1.0001), std::exception);
  /* Set a weight to a kernel that does not exist on the graph */
  CPPUNIT_ASSERT_THROW((*d_graph)[not_connected]->set_weight (0.7),
                       std::exception);

  (*d_graph)[source0]->set_weight (0.1);
  (*d_graph)[source1]->set_weight (0.1);
  (*d_graph)[inout]->set_weight (0.4);

  std::cout << (*d_graph) << std::endl;

}

/**
 * Tests the topological sorting capabilities of the graph
 * using a simple test case, where all kernels are in a row starting
 * from one source
 */
void
test_connection_graph::test_topological_sort_simple ()
{
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr a = testing::source::make_shared ("source_a");
  testing::in_out::sptr b = testing::in_out::make_shared ("b");
  testing::in_out::sptr c = testing::in_out::make_shared ("c");
  testing::in_out::sptr d = testing::in_out::make_shared ("d");
  testing::in_out::sptr e = testing::in_out::make_shared ("e");
  testing::sink::sptr f = testing::sink::make_shared ("sink_f");

  graph->add_connection ((*a)["out0"] >> (*b)["in0"]);
  graph->add_connection ((*b)["out0"] >> (*c)["in0"]);
  graph->add_connection ((*c)["out0"] >> (*d)["in0"]);
  graph->add_connection ((*d)["out0"] >> (*e)["in0"]);
  graph->add_connection ((*e)["out0"] >> (*f)["in0"]);

  graph->analyze ();
  CPPUNIT_ASSERT(graph->is_analyzed ());
  CPPUNIT_ASSERT(graph->size () == 6);

  /* This graph has only one connected component */
  CPPUNIT_ASSERT(graph->get_connected_components_num () == 1);
  CPPUNIT_ASSERT(graph->get_connected_component_size (0) == graph->size ());

  /* This is a quite easy topology with only one possible solution */
  std::deque<kernel::sptr> l = graph->get_connected_component_kernels (0);
  CPPUNIT_ASSERT(l[0] == a);
  CPPUNIT_ASSERT(l[1] == b);
  CPPUNIT_ASSERT(l[2] == c);
  CPPUNIT_ASSERT(l[3] == d);
  CPPUNIT_ASSERT(l[4] == e);
  CPPUNIT_ASSERT(l[5] == f);
}

void
test_connection_graph::test_connected_components ()
{
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr a = testing::source::make_shared ("source_a");
  testing::in_out::sptr b = testing::in_out::make_shared ("b");
  testing::in_out::sptr c = testing::in_out::make_shared ("c");
  testing::sink::sptr d = testing::sink::make_shared ("sink_d");
  testing::source::sptr e = testing::source::make_shared ("source_e");
  testing::in_out::sptr f = testing::in_out::make_shared ("f");
  testing::sink::sptr g = testing::sink::make_shared ("sink_g");

  graph->add_connection ((*a)["out0"] >> (*b)["in0"]);
  graph->add_connection ((*b)["out0"] >> (*c)["in0"]);
  graph->add_connection ((*c)["out0"] >> (*d)["in0"]);

  graph->add_connection ((*e)["out0"] >> (*f)["in0"]);
  graph->add_connection ((*f)["out0"] >> (*g)["in0"]);

  graph->analyze ();
  CPPUNIT_ASSERT(graph->is_analyzed ());
  CPPUNIT_ASSERT(graph->size () == 7);
  CPPUNIT_ASSERT(graph->get_connected_components_num () == 2);

  /* This is a quite easy topology with only one possible solution */
  std::deque<kernel::sptr> l0 = graph->get_connected_component_kernels (0);
  std::deque<kernel::sptr> l1 = graph->get_connected_component_kernels (1);

  /* The smallest connected component should be always be first */
  CPPUNIT_ASSERT(graph->get_connected_component_size (0) == 3);
  CPPUNIT_ASSERT(graph->get_connected_component_size (1) == 4);

  CPPUNIT_ASSERT(l0[0] == e);
  CPPUNIT_ASSERT(l0[1] == f);
  CPPUNIT_ASSERT(l0[2] == g);

  CPPUNIT_ASSERT(l1[0] == a);
  CPPUNIT_ASSERT(l1[1] == b);
  CPPUNIT_ASSERT(l1[2] == c);
  CPPUNIT_ASSERT(l1[3] == d);
}

}  // namespace core

}  // namespace eqnx

