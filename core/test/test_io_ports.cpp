/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory>
#include <equinox/core/mm/msg_queue_simple.h>
#include <equinox/core/mm/mem_pool_simple.h>

#include "test_io_ports.h"

namespace eqnx
{

void
test_io_ports::setUp ()
{
  d_mq = core::msg_queue_simple::make_shared ("test", 1024, 2);
  d_mp = core::mem_pool_simple::make_shared (1024, 1024);

  /* No kernel is available here. Pass a nullptr */
  d_in0 = input_port::make_shared ("in0");
  d_in1 = input_port::make_shared ("in1");
  d_out = output_port::make_shared ("out0", 1024, 1024);

  d_in0->setup (d_mq);
  d_in1->setup (d_mq);
  d_out->setup (d_mp, d_mq);
}

void
test_io_ports::tearDown ()
{
  d_mq = nullptr;
}

void
test_io_ports::test_produce_consume ()
{
  for (size_t i = 0; i < 1000; i++) {
    msg::sptr m = d_out->new_msg ();
    CPPUNIT_ASSERT(m != nullptr);
    d_mq->push (m);
    d_in0->read ();
    d_in1->read ();
  }
}

}  // namespace eqnx

