/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_INNER_SCHED_H_
#define CORE_TEST_TEST_INNER_SCHED_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

namespace eqnx
{

namespace core
{
class test_inner_sched : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE (test_inner_sched);
  CPPUNIT_TEST(test_inner_sched_rr);CPPUNIT_TEST_SUITE_END ()
  ;
public:

  void
  test_inner_sched_rr ();

  void
  test_inner_sched_rr_scoped_exit ();

};
}  // namespace core

}  // namespace eqnx

#endif /* CORE_TEST_TEST_INNER_SCHED_H_ */
