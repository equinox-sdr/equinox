/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_outer_sched.h"
#include <equinox/runtime/exception.h>
#include <equinox/kernels/testing/source.h>
#include <equinox/kernels/testing/sink.h>
#include <equinox/kernels/testing/in_out.h>

namespace eqnx
{

namespace core
{

void
test_outer_sched::create_connections ()
{
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr a = testing::source::make_shared ("source_a");
  testing::in_out::sptr b = testing::in_out::make_shared ("b");
  testing::in_out::sptr c = testing::in_out::make_shared ("c");
  testing::sink::sptr d = testing::sink::make_shared ("sink_d");

  graph->add_connection ((*a)["out0"] >> (*b)["in0"]);
  graph->add_connection ((*b)["out0"] >> (*c)["in0"]);
  graph->add_connection ((*c)["out0"] >> (*d)["in0"]);


  d_os = new outer_sched<load_balancer_spectral, inner_sched_rr>(1, graph);
  d_os->start();
  std::thread t(&test_outer_sched::stop_outer_sched, this);
  t.join();
  d_os->join();
  delete d_os;
}

void
test_outer_sched::stop_outer_sched ()
{
  std::this_thread::sleep_for(std::chrono::seconds(5));
  d_os->stop();
  std::cout << "Exit" << std::endl;
}

}  // namespace core

}  // namespace eqnx
