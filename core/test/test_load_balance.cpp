/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_load_balance.h"
#include <equinox/core/sched/load_balancer.h>
#include <equinox/core/sched/load_balancer_simple.h>
#include <equinox/core/sched/load_balancer_spectral.h>
#include <equinox/core/sched/connection_graph.h>
#include <equinox/kernels/testing/sink.h>
#include <equinox/kernels/testing/source.h>
#include <equinox/kernels/testing/in_out.h>

namespace eqnx
{

namespace core
{

void
test_load_balance::test_load_balancer_simple ()
{
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr a = testing::source::make_shared ("source_a");
  testing::in_out::sptr b = testing::in_out::make_shared ("b");
  testing::in_out::sptr c = testing::in_out::make_shared ("c");
  testing::sink::sptr d = testing::sink::make_shared ("sink_d");
  testing::source::sptr e = testing::source::make_shared ("source_e");
  testing::in_out::sptr f = testing::in_out::make_shared ("f");
  testing::sink::sptr g = testing::sink::make_shared ("sink_g");

  graph->add_connection ((*a)["out0"] >> (*b)["in0"]);
  graph->add_connection ((*b)["out0"] >> (*c)["in0"]);
  graph->add_connection ((*c)["out0"] >> (*d)["in0"]);

  graph->add_connection ((*e)["out0"] >> (*f)["in0"]);
  graph->add_connection ((*f)["out0"] >> (*g)["in0"]);

  /*
   * With 2 connected components, the load balancer should create
   * at least 2 workers
   */
  load_balancer *lb = new load_balancer_simple (graph, 10);
  lb->analyze ();

  CPPUNIT_ASSERT(graph->get_connected_components_num () == 2);
  CPPUNIT_ASSERT(lb->actual_workers_num () > 2);

  for (size_t i = 0; i < lb->actual_workers_num (); i++) {
    auto x = lb->get_worker_assignement (i);
    for (kernel::sptr k : x) {
      std::cout << "Worker " << i << " " << k->name () << std::endl;
    }
  }
  delete lb;
}

static void
test_spectral (size_t k, size_t min_partition)
{
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr n1 = testing::source::make_shared ("1");
  testing::in_out::sptr n2 = testing::in_out::make_shared ("2", 1, 1);
  testing::in_out::sptr n3 = testing::in_out::make_shared ("3", 2, 1);
  testing::in_out::sptr n4 = testing::in_out::make_shared ("4", 1, 2);
  testing::in_out::sptr n5 = testing::in_out::make_shared ("5", 2, 1);
  testing::in_out::sptr n6 = testing::in_out::make_shared ("6", 1, 1);
  testing::sink::sptr n7 = testing::sink::make_shared ("7");
  testing::sink::sptr n8 = testing::sink::make_shared ("8");
  graph->add_connection ((*n1)["out0"] >> (*n2)["in0"]);
  graph->add_connection ((*n1)["out0"] >> (*n4)["in0"]);
  graph->add_connection ((*n2)["out0"] >> (*n3)["in0"]);
  graph->add_connection ((*n3)["out0"] >> (*n5)["in0"]);
  graph->add_connection ((*n4)["out0"] >> (*n3)["in1"]);
  graph->add_connection ((*n4)["out1"] >> (*n5)["in1"]);
  graph->add_connection ((*n5)["out0"] >> (*n6)["in0"]);
  graph->add_connection ((*n6)["out0"] >> (*n7)["in0"]);
  graph->add_connection ((*n6)["out0"] >> (*n8)["in0"]);

  load_balancer *lb = new load_balancer_spectral (graph, k, min_partition);
  bool res = lb->analyze ();
  CPPUNIT_ASSERT(res);
  std::cout << "Workers (requested)   : " << k << std::endl;
  std::cout << "Workers (computed)    : " << lb->actual_workers_num ()
      << std::endl;
  std::cout << "Minimum partition size: " << min_partition << std::endl;

  for (size_t i = 0; i < lb->actual_workers_num (); i++) {
    std::cout << "Worker " << i << " assignment:" << std::endl;
    for (kernel::sptr k : lb->get_worker_assignement (i)) {
      std::cout << *k << std::endl;
    }
    std::cout << std::endl;
  }
  delete lb;
}

void
test_load_balance::test_load_balancer_spectral ()
{
  for (size_t i = 1; i < 10; i++) {
    for (size_t j = 0; j < 5; j++) {
      test_spectral (i, j);
    }

  }
}

/**
 * Two connected components into two workers. Should be easy!
 */
void
test_load_balance::test_load_balancer_spectral_extr_case1 ()
{
  std::cout << "Extreme case 1" << std::endl;
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr n1 = testing::source::make_shared ("1");
  testing::in_out::sptr n2 = testing::in_out::make_shared ("2", 1, 1);
  testing::in_out::sptr n3 = testing::in_out::make_shared ("3", 2, 1);
  testing::in_out::sptr n4 = testing::in_out::make_shared ("4", 1, 2);
  testing::sink::sptr n5 = testing::sink::make_shared ("5", 2);
  testing::source::sptr n6 = testing::source::make_shared ("6", 1);
  testing::sink::sptr n7 = testing::sink::make_shared ("7");
  testing::sink::sptr n8 = testing::sink::make_shared ("8");
  /* Connected component 0 */
  graph->add_connection ((*n1)["out0"] >> (*n2)["in0"]);
  graph->add_connection ((*n1)["out0"] >> (*n4)["in0"]);
  graph->add_connection ((*n2)["out0"] >> (*n3)["in0"]);
  graph->add_connection ((*n3)["out0"] >> (*n5)["in0"]);
  graph->add_connection ((*n4)["out0"] >> (*n3)["in1"]);
  graph->add_connection ((*n4)["out1"] >> (*n5)["in1"]);

  /* Connected component 1 */
  graph->add_connection ((*n6)["out0"] >> (*n7)["in0"]);
  graph->add_connection ((*n6)["out0"] >> (*n8)["in0"]);

  load_balancer *lb = new load_balancer_spectral (graph, 2, 2);
  bool res = lb->analyze ();
  CPPUNIT_ASSERT(res);

  for (size_t i = 0; i < lb->actual_workers_num (); i++) {
    std::cout << "Worker " << i << " assignment:" << std::endl;
    for (kernel::sptr k : lb->get_worker_assignement (i)) {
      std::cout << *k << std::endl;
    }
    std::cout << std::endl;
  }
  delete lb;
}

/**
 * Three connected components, 3 workers. Should produce exception!
 */
void
test_load_balance::test_load_balancer_spectral_extr_case2 ()
{
  std::cout << "Extreme case 2" << std::endl;
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr n1 = testing::source::make_shared ("1");
  testing::in_out::sptr n2 = testing::in_out::make_shared ("2", 1, 1);
  testing::sink::sptr n3 = testing::sink::make_shared ("3", 2);

  testing::source::sptr n4 = testing::source::make_shared ("4", 1);
  testing::sink::sptr n5 = testing::sink::make_shared ("5", 2);

  testing::source::sptr n6 = testing::source::make_shared ("6", 1);
  testing::sink::sptr n7 = testing::sink::make_shared ("7");
  testing::sink::sptr n8 = testing::sink::make_shared ("8");

  /* Connected component 0 */
  graph->add_connection ((*n1)["out0"] >> (*n2)["in0"]);
  graph->add_connection ((*n1)["out0"] >> (*n3)["in0"]);
  graph->add_connection ((*n2)["out0"] >> (*n3)["in1"]);

  /* Connected component 1 */
  graph->add_connection ((*n4)["out0"] >> (*n5)["in0"]);

  /* Connected component 2 */
  graph->add_connection ((*n6)["out0"] >> (*n7)["in0"]);
  graph->add_connection ((*n6)["out0"] >> (*n8)["in0"]);

  load_balancer *lb = new load_balancer_spectral (graph, 2, 2);

  CPPUNIT_ASSERT_THROW(bool res = lb->analyze(), std::exception);

  delete lb;
}

void
test_load_balance::test_load_balancer_spectral_extr_case3 ()
{
  std::cout << "Extreme case 3" << std::endl;
  connection_graph::sptr graph = connection_graph::make_shared ();
  testing::source::sptr n1 = testing::source::make_shared ("1");
  testing::in_out::sptr n2 = testing::in_out::make_shared ("2", 1, 1);
  testing::sink::sptr n3 = testing::sink::make_shared ("3", 2);

  testing::source::sptr n4 = testing::source::make_shared ("4", 1);
  testing::sink::sptr n5 = testing::sink::make_shared ("5", 2);

  testing::source::sptr n6 = testing::source::make_shared ("6", 1);
  testing::sink::sptr n7 = testing::sink::make_shared ("7");
  testing::sink::sptr n8 = testing::sink::make_shared ("8");

  /* Connected component 0 */
  graph->add_connection ((*n1)["out0"] >> (*n2)["in0"]);
  graph->add_connection ((*n1)["out0"] >> (*n3)["in0"]);
  graph->add_connection ((*n2)["out0"] >> (*n3)["in1"]);

  /* Connected component 1 */
  graph->add_connection ((*n4)["out0"] >> (*n5)["in0"]);

  /* Connected component 2 */
  graph->add_connection ((*n6)["out0"] >> (*n7)["in0"]);
  graph->add_connection ((*n6)["out0"] >> (*n8)["in0"]);

  load_balancer *lb = new load_balancer_spectral (graph, 3, 2);
  bool res = lb->analyze ();
  CPPUNIT_ASSERT(res);
  CPPUNIT_ASSERT(lb->actual_workers_num () == 3);

  for (size_t i = 0; i < lb->actual_workers_num (); i++) {
    std::cout << "Worker " << i << " assignment:" << std::endl;
    for (kernel::sptr k : lb->get_worker_assignement (i)) {
      std::cout << *k << std::endl;
    }
    std::cout << std::endl;
  }
  delete lb;
}

}  // namespace core

}  // namespace eqnx

