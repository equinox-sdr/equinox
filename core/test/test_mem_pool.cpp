/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/mem_pool_simple.h>
#include <equinox/core/mm/mem_pool_r.h>

#include "test_mem_pool.h"

#define MSG_NUM_EXCHANGE 1000
namespace eqnx
{

namespace core
{

void
test_mem_pool::test_mem_pool_simple ()
{
  msg::sptr msgs[2048];

  /* Demonstrate the shared pointers automatic memory release */
  for (size_t i = 0; i < 4 * 2048; i++) {
    msgs[i % 2048] = d_mp_simple->new_msg ();
  }
}

void
test_mem_pool::test_mem_pool_r ()
{
  /*
   * De-allocation of smart pointer should be possible, so we actually
   * indirectly leave one free mem block on the pool
   */
  msg::sptr msgs[2048];

  /* Demonstrate the shared pointers automatic memory release */
  for (size_t i = 0; i < 4 * 2048; i++) {
    msgs[i % 2048] = d_mp_r->new_msg ();
  }
}

void
test_mem_pool::setUp ()
{
  d_mp_simple = mem_pool_simple::make_shared (4096, 4096);
  d_mp_r = mem_pool_r::make_shared (4096, 4096);
}

void
test_mem_pool::tearDown ()
{
}

}  // namespace core

}  // namespace eqnx
