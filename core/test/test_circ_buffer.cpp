/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cppunit/TestCase.h>

#include "test_circ_buffer.h"

namespace eqnx
{

namespace core
{

void
test_circ_buffer::setUp ()
{
  d_int_circ = new circ_buffer<int> (4, 8);
  CPPUNIT_ASSERT(d_int_circ->space () == 7);
  CPPUNIT_ASSERT(d_int_circ->cnt () == 0);
}

void
test_circ_buffer::tearDown ()
{
  delete d_int_circ;
}

void
test_circ_buffer::test_allocation_power_of_2 ()
{
  circ_buffer<int> b = circ_buffer<int> (4, 512);
  CPPUNIT_ASSERT(b.space () == 511);
}

void
test_circ_buffer::test_allocation_non_power_of_2 ()
{
  circ_buffer<int> b = circ_buffer<int> (4, 513);
  CPPUNIT_ASSERT(b.space () == 1023);
}

void
test_circ_buffer::test_empty ()
{
  circ_buffer<int> b = circ_buffer<int> (4, 4);
  CPPUNIT_ASSERT(b.cnt () == 0);
  CPPUNIT_ASSERT(b.empty ());
}

void
test_circ_buffer::test_full ()
{
  circ_buffer<int> b = circ_buffer<int> (4, 4);
  b.push (1);
  b.push (2);
  b.push (3);
  CPPUNIT_ASSERT(b.cnt () == 3);
  CPPUNIT_ASSERT(b.full ());
}

void
test_circ_buffer::test_push_pop ()
{
  int elem;
  circ_buffer<int> b = circ_buffer<int> (1, 4);
  CPPUNIT_ASSERT(b.space () == 3);
  b.push (1);
  CPPUNIT_ASSERT(b.cnt () == 1);
  CPPUNIT_ASSERT(b.cnt (0) == 1);
  CPPUNIT_ASSERT(b.space () == 2);

  b.push (2);
  CPPUNIT_ASSERT(b.cnt () == 2);
  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(b.space () == 1);

  b.push (3);
  CPPUNIT_ASSERT(b.cnt () == 3);
  CPPUNIT_ASSERT(b.cnt (0) == 3);
  CPPUNIT_ASSERT(b.space () == 0);

  elem = b.pop (0);
  CPPUNIT_ASSERT(b.cnt () == 2);
  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(elem == 1);
  CPPUNIT_ASSERT(b.space () == 1);

  elem = b.pop (0);
  CPPUNIT_ASSERT(b.cnt () == 1);
  CPPUNIT_ASSERT(b.cnt (0) == 1);
  CPPUNIT_ASSERT(elem == 2);
  CPPUNIT_ASSERT(b.space () == 2);

  /* Push again to force a wrap-around*/
  b.push (4);
  CPPUNIT_ASSERT(b.cnt () == 2);
  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(b.space () == 1);

  b.push (5);
  CPPUNIT_ASSERT(b.cnt () == 3);
  CPPUNIT_ASSERT(b.cnt (0) == 3);
  CPPUNIT_ASSERT(b.full ());
  CPPUNIT_ASSERT(b.space () == 0);

  elem = b.pop (0);
  CPPUNIT_ASSERT(b.cnt () == 2);
  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(elem == 3);
  CPPUNIT_ASSERT(b.space () == 1);

  elem = b.pop (0);
  CPPUNIT_ASSERT(b.cnt () == 1);
  CPPUNIT_ASSERT(b.cnt (0) == 1);
  CPPUNIT_ASSERT(elem == 4);
  CPPUNIT_ASSERT(b.space () == 2);

  elem = b.pop (0);
  CPPUNIT_ASSERT(b.cnt () == 0);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
  CPPUNIT_ASSERT(elem == 5);
  CPPUNIT_ASSERT(b.space () == 3);
  CPPUNIT_ASSERT(b.empty ());
  CPPUNIT_ASSERT(b.empty (0));
}

void
test_circ_buffer::test_push_all_pop ()
{
  int elem;
  circ_buffer<int> b = circ_buffer<int> (1, 4);
  b.push (1);
  b.push (2);
  b.push (3);
  CPPUNIT_ASSERT(b.full ());

  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 1);
  CPPUNIT_ASSERT(b.space () == 1);
  CPPUNIT_ASSERT(b.cnt () == 2);
  CPPUNIT_ASSERT(b.cnt (0) == 2);

  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 2);
  CPPUNIT_ASSERT(b.space () == 2);
  CPPUNIT_ASSERT(b.cnt () == 1);
  CPPUNIT_ASSERT(b.cnt (0) == 1);

  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 3);
  CPPUNIT_ASSERT(b.space () == 3);
  CPPUNIT_ASSERT(b.cnt () == 0);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
}

void
test_circ_buffer::test_multiple_readers ()
{
  int elem;
  circ_buffer<int> b = circ_buffer<int> (4, 4);
  CPPUNIT_ASSERT(b.get_readers_num () == 4);

  b.push (10);
  CPPUNIT_ASSERT(b.cnt (0) == 1);
  CPPUNIT_ASSERT(b.cnt (1) == 1);
  CPPUNIT_ASSERT(b.cnt (2) == 1);
  CPPUNIT_ASSERT(b.cnt (3) == 1);

  b.push (20);
  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(b.cnt (1) == 2);
  CPPUNIT_ASSERT(b.cnt (2) == 2);
  CPPUNIT_ASSERT(b.cnt (3) == 2);

  /* Reader 1 consumes */
  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 10);
  CPPUNIT_ASSERT(b.cnt (0) == 1);
  CPPUNIT_ASSERT(b.cnt (1) == 2);
  CPPUNIT_ASSERT(b.cnt (2) == 2);
  CPPUNIT_ASSERT(b.cnt (3) == 2);

  b.push (30);
  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(b.cnt (1) == 3);
  CPPUNIT_ASSERT(b.cnt (2) == 3);
  CPPUNIT_ASSERT(b.cnt (3) == 3);
  CPPUNIT_ASSERT(b.space () == 0);
  CPPUNIT_ASSERT(b.cnt () == 3);
  CPPUNIT_ASSERT(b.full ());

  /* Pop other readers and check if global tail propagated correctly */
  elem = b.pop (1);
  CPPUNIT_ASSERT(elem == 10);
  elem = b.pop (2);
  CPPUNIT_ASSERT(elem == 10);
  elem = b.pop (3);
  CPPUNIT_ASSERT(elem == 10);

  CPPUNIT_ASSERT(b.cnt (0) == 2);
  CPPUNIT_ASSERT(b.cnt (1) == 2);
  CPPUNIT_ASSERT(b.cnt (2) == 2);
  CPPUNIT_ASSERT(b.cnt (3) == 2);
  CPPUNIT_ASSERT(b.space () == 1);
  CPPUNIT_ASSERT(b.cnt () == 2);

  /* Empty reader 0 */
  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 20);
  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 30);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
  CPPUNIT_ASSERT(b.cnt (1) == 2);
  CPPUNIT_ASSERT(b.cnt (2) == 2);
  CPPUNIT_ASSERT(b.cnt (3) == 2);
  CPPUNIT_ASSERT(b.space () == 1);
  CPPUNIT_ASSERT(b.cnt () == 2);

  /* Empty reader 1 */
  elem = b.pop (1);
  CPPUNIT_ASSERT(elem == 20);
  elem = b.pop (1);
  CPPUNIT_ASSERT(elem == 30);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
  CPPUNIT_ASSERT(b.cnt (1) == 0);
  CPPUNIT_ASSERT(b.cnt (2) == 2);
  CPPUNIT_ASSERT(b.cnt (3) == 2);
  CPPUNIT_ASSERT(b.space () == 1);
  CPPUNIT_ASSERT(b.cnt () == 2);

  /* Push again */
  b.push (50);
  /* Empty reader 0 and 1 */
  elem = b.pop (0);
  CPPUNIT_ASSERT(elem == 50);
  elem = b.pop (1);
  CPPUNIT_ASSERT(elem == 50);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
  CPPUNIT_ASSERT(b.cnt (1) == 0);
  CPPUNIT_ASSERT(b.cnt (2) == 3);
  CPPUNIT_ASSERT(b.cnt (3) == 3);
  CPPUNIT_ASSERT(b.space () == 0);
  CPPUNIT_ASSERT(b.cnt () == 3);

  /* Dequeue one element from 3 and 4 */
  elem = b.pop (2);
  CPPUNIT_ASSERT(elem == 20);
  elem = b.pop (3);
  CPPUNIT_ASSERT(elem == 20);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
  CPPUNIT_ASSERT(b.cnt (1) == 0);
  CPPUNIT_ASSERT(b.cnt (2) == 2);
  CPPUNIT_ASSERT(b.cnt (3) == 2);
  CPPUNIT_ASSERT(b.space () == 1);
  CPPUNIT_ASSERT(b.cnt () == 2);

  /* Dequeue one element from 3 and 4 */
  elem = b.pop (2);
  CPPUNIT_ASSERT(elem == 30);
  elem = b.pop (3);
  CPPUNIT_ASSERT(elem == 30);
  CPPUNIT_ASSERT(b.cnt (0) == 0);
  CPPUNIT_ASSERT(b.cnt (1) == 0);
  CPPUNIT_ASSERT(b.cnt (2) == 1);
  CPPUNIT_ASSERT(b.cnt (3) == 1);
  CPPUNIT_ASSERT(b.space () == 2);
  CPPUNIT_ASSERT(b.cnt () == 1);

}

}  // namespace core

}  // namespace eqnx
