/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <thread>
#include <benchmark/benchmark.h>

#include "bench_msg_queue.h"

BENCHMARK_DEFINE_F(bench_msg_queue, produce_consume_lock)(benchmark::State &state)
{
  eqnx::core::mem_pool *mp = new eqnx::core::mem_pool_r(2048, 1024);
  d_q = new eqnx::core::msg_queue_r("test", 1024, 1);

  while(state.KeepRunning()) {
    for(int i = 0; i < state.range(0); i++) {
      d_q->push(mp->new_msg());
      eqnx::msg::sptr m = d_q->pop(0);
    }
  }
  delete d_q;
  delete mp;
}

BENCHMARK_DEFINE_F(bench_msg_queue, produce_consume_threaded)(benchmark::State &state)
{
  const size_t msg_num = state.range(0);

  /* Producer constructs and destructs the message queue */
  if(state.thread_index == 0) {
    eqnx::core::mem_pool *mp = new eqnx::core::mem_pool_r(2048, 1024);
    d_q = new eqnx::core::msg_queue_r (
        "test", 1024, state.threads - 1);

    while(state.KeepRunning()) {
      auto start = std::chrono::high_resolution_clock::now();
      for(size_t i = 0; i < msg_num; i++) {
        d_q->push(mp->new_msg());
      }
      auto end = std::chrono::high_resolution_clock::now();
      auto diff = std::chrono::duration_cast<std::chrono::duration<double>> (
          end - start);
      state.SetIterationTime(diff.count());
    }
    delete d_q;
    delete mp;
  }
  else{
    /* Consumers */
    while(state.KeepRunning()) {
      for(size_t i = 0; i < msg_num; i++) {
        d_q->pop((size_t) (state.thread_index - 1));
      }
    }
  }
}

BENCHMARK_DEFINE_F(bench_msg_queue, produce_consume_simple)(benchmark::State &state)
{
  d_q = new eqnx::core::msg_queue_simple ("test", 1024, 1);
  eqnx::core::mem_pool *mp = new eqnx::core::mem_pool_r(2048, 1024);

  while (state.KeepRunning ()) {
    for (int i = 0; i < state.range (0); i++) {
      d_q->push (mp->new_msg());
      d_q->pop (0);
    }
  }
  delete d_q;
  delete mp;
}

BENCHMARK_REGISTER_F(bench_msg_queue, produce_consume_lock)
              ->Repetitions(4)
              ->Range(1000000, 1000000);
BENCHMARK_REGISTER_F(bench_msg_queue, produce_consume_simple)
              ->Repetitions(4)
              ->Range(1000000, 1000000);
BENCHMARK_REGISTER_F(bench_msg_queue, produce_consume_threaded)
              ->DenseThreadRange(2, std::thread::hardware_concurrency(), 1)
              ->Repetitions(4)
              ->UseRealTime()
              ->Range(1000000, 1000000);

BENCHMARK_MAIN();

