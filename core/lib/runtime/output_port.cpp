/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/output_port.h>
#include <equinox/core/sched/connection.h>

namespace eqnx
{

output_port::sptr
output_port::make_shared (const std::string& name, size_t capacity,
                          size_t msg_size)
{
  return std::shared_ptr<output_port> (
      new output_port (name, capacity, msg_size));
}

output_port::output_port (const std::string& name, size_t capacity,
                          size_t msg_size) :
        io_port (name, IO_PORT_DIR_OUTPUT, false),
        d_capacity (capacity),
        d_msg_size (msg_size),
        d_mq (nullptr),
        d_mp (nullptr)
{
}

output_port::~output_port ()
{
  if (d_mq) {
    d_mq->clear ();
  }
}

void
output_port::setup (core::mem_pool::sptr mp, core::msg_queue::sptr mq)
{
  d_mq = mq;
  d_mp = mp;
  d_is_ready = true;
}

msg::sptr
output_port::new_msg ()
{
  return d_mp->new_msg ();
}

size_t
output_port::capacity ()
{
  return d_capacity;
}

size_t
output_port::msg_size ()
{
  return d_msg_size;
}

bool
output_port::full()
{
  return d_mq->full() || d_mp->full();
}

void
output_port::stop ()
{
  d_mq->clear ();
  d_mp = nullptr;
  d_mq = nullptr;
}

template<typename _Rep>
  inline int
  output_port::write (msg::sptr m, const std::chrono::duration<_Rep>& timeout)
  {
    return d_mq->push_timedwait(m, std::chrono::microseconds(timeout).count());
  }

int
output_port::write (msg::sptr m)
{
  return d_mq->push(m);
}

}  // namespace eqnx

