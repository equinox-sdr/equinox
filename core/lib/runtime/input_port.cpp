/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/input_port.h>

namespace eqnx
{

input_port::sptr
input_port::make_shared (const std::string& name)
{
  return std::shared_ptr<input_port> (new input_port (name));
}

input_port::input_port (const std::string& name) :
        io_port (name, io_port::IO_PORT_DIR_INPUT, false),
        d_msg_consumer (nullptr)
{
}

/**
 * Configures the input port. Under the hood, this method registers the input
 * port as a consumer the the message queue \a msgq
 * @param msgq the message queue to register for incoming messages
 */
void
input_port::setup (core::msg_queue::sptr msgq)
{
  if (d_is_ready) {
    throw std::runtime_error ("setup() can be called only once");
  }

  d_msg_consumer = msgq->register_consumer(name());
  d_is_ready = true;
}

msg::sptr
input_port::read ()
{
  /*
   * If the setup() has not been called, null pointer exceptions will be
   * raised. In normal situations this can never happen, because the
   * setup() is called internally by the Equinox back-end and never from
   * the user.
   */
  return d_msg_consumer->read ();
}


bool
input_port::empty()
{
  return d_msg_consumer->empty();
}

template<typename _Rep>
  inline msg::sptr
  input_port::read (const std::chrono::duration<_Rep>& timeout)
  {
    /*
     * If the setup() has not been called, null pointer exceptions will be
     * raised. In normal situations this can never happen, because the
     * setup() is called internally by the Equinox back-end and never from
     * the user.
     */
    return d_msg_consumer->read (timeout);
  }

}  // namespace eqnx
