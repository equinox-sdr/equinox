/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/msg_queue.h>

namespace eqnx
{

namespace core
{

msg_queue::~msg_queue ()
{
}

/**
 *
 * @return the number of active consumers of the message queue
 */
size_t
msg_queue::get_consumers_num ()
{
  return d_consumers_num;
}

/**
 *
 * @return the mnemonic name of the message queue
 */
const std::string
msg_queue::name ()
{
  return d_name;
}

/**
 * Registers a new consumer at the message queue.
 *
 * @note This method is thread safe
 * @param name the name of the consumer
 * @return a unique pointer of ::msg_queue_consumer
 */
msg_queue_consumer::uptr
msg_queue::register_consumer (const std::string& name)
{
  std::unique_lock<std::mutex> lock(d_cossumers_lock);
  if (d_consumer_idx >= d_consumers_num) {
    return nullptr;
  }
  return msg_queue_consumer::make (name, d_consumer_idx++, this->get_sptr ());
}

/**
 *
 * @return the shared pointer of the queue
 */
msg_queue::sptr
msg_queue::get_sptr ()
{
  return shared_from_this ();
}

void
msg_queue::stop ()
{
  d_stop = true;
}

msg_queue::msg_queue (const std::string& name, size_t size, size_t consumers,
                      bool allow_coalescing, bool profiling_mode) :
        d_name (name),
        d_consumers_num (consumers),
        d_coallescing (allow_coalescing),
        d_profiling (profiling_mode),
        d_stop (false),
        d_q (consumers, size),
        d_consumer_idx (0)
{
}

}  // namespace core

}  // namespace eqnx
