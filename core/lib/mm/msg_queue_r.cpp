/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/msg_queue_exception.h>
#include <equinox/core/mm/msg_queue_r.h>

#include <thread>

namespace eqnx
{

namespace core
{
msg_queue::sptr
msg_queue_r::make_shared (const std::string& name, size_t size,
                             size_t consumers, bool allow_coalescing,
                             bool profiling_mode)
{
  return std::shared_ptr<msg_queue> (
      new msg_queue_r (name, size, consumers, allow_coalescing,
                          profiling_mode));
}

msg_queue_r::msg_queue_r (const std::string& name, size_t size,
                                size_t consumers, bool allow_coalescing,
                                bool profiling_mode) :
        msg_queue (name, size, consumers, allow_coalescing, profiling_mode)
{
}

msg_queue_r::~msg_queue_r ()
{
}

int
msg_queue_r::push (msg::sptr msg)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  while (d_q.full ()) {
    if (d_stop) {
      throw msg_queue_exception ();
    }
    d_cv.wait (lock);
  }
  d_q.push (msg);
  lock.unlock ();
  d_cv.notify_all ();
  return EQNX_MSGQ_NO_ERROR;
}

int
msg_queue_r::push_nowait (msg::sptr msg)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  if (d_q.full ()) {
    if (d_stop) {
      throw msg_queue_exception ();
    }
    return EQNX_MSGQ_NO_MEM;
  }
  d_q.push (msg);
  lock.unlock ();
  d_cv.notify_all ();
  return EQNX_MSGQ_NO_ERROR;
}

int
msg_queue_r::push_timedwait (msg::sptr msg, size_t timeout_us)
{
  std::cv_status ret;
  std::unique_lock<std::mutex> lock (d_mtx);
  while (d_q.full ()) {
    if (d_stop) {
      throw msg_queue_exception ();
    }
    ret = d_cv.wait_for (lock, std::chrono::microseconds (timeout_us));
    if (ret == std::cv_status::timeout) {
      return EQNX_MSGQ_TIMER_EXPIRED;
    }
  }
  d_q.push (msg);
  lock.unlock ();
  d_cv.notify_all ();
  return EQNX_MSGQ_NO_ERROR;
}

msg::sptr
msg_queue_r::pop (size_t reader)
{
  msg::sptr m;
  std::unique_lock<std::mutex> lock (d_mtx);
  while (d_q.empty (reader)) {
    if (d_stop) {
      throw msg_queue_exception ();
    }
    d_cv.wait (lock);
  }
  m = d_q.pop (reader);
  lock.unlock ();
  d_cv.notify_all ();
  return m;
}

msg::sptr
msg_queue_r::pop_timedwait (size_t reader, size_t timeout_us)
{
  msg::sptr m;
  std::cv_status ret;
  std::unique_lock<std::mutex> lock (d_mtx);
  while (d_q.empty (reader)) {
    if (d_stop) {
      throw msg_queue_exception ();
    }
    ret = d_cv.wait_for (lock, std::chrono::microseconds (timeout_us));
    if (ret == std::cv_status::timeout) {
      return nullptr;
    }
  }
  m = d_q.pop (reader);
  lock.unlock ();
  d_cv.notify_all ();
  return m;
}

msg::sptr
msg_queue_r::pop_nowait (size_t reader)
{
  msg::sptr m;
  std::unique_lock<std::mutex> lock (d_mtx);
  if (d_q.empty (reader)) {
    return nullptr;
  }
  m = d_q.pop (reader);
  lock.unlock ();
  d_cv.notify_all ();
  return m;
}

void
msg_queue_r::clear ()
{
  std::unique_lock<std::mutex> lock (d_mtx);
  while (!d_q.empty ()) {
    for (size_t i = 0; i < d_consumers_num; i++) {
      if (!d_q.empty (i)) {
        msg::sptr x = d_q.pop (i);
      }
    }
  }
  lock.unlock ();
  d_cv.notify_all ();
}

bool
msg_queue_r::empty ()
{
  std::unique_lock<std::mutex> lock (d_mtx);
  return d_q.empty();
}

bool
msg_queue_r::empty (size_t reader)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  return d_q.empty(reader);
}

bool
msg_queue_r::full ()
{
  std::unique_lock<std::mutex> lock (d_mtx);
  return d_q.full();
}

}  // namespace core

}  // namespace eqnx
