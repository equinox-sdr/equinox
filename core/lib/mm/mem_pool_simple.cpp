/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/mem_pool_simple.h>

namespace eqnx
{

namespace core
{

mem_pool::sptr
mem_pool_simple::make_shared (size_t msg_num, size_t max_msg_size)
{
  return std::shared_ptr<mem_pool> (new mem_pool_simple (msg_num, max_msg_size));
}

mem_pool_simple::mem_pool_simple (size_t msg_num, size_t max_msg_size) :
        mem_pool (msg_num, max_msg_size)
{
  for (size_t i = 0; i < msg_num; i++) {
    d_blocks[i].ptr = new uint8_t[max_msg_size];
  }
}

mem_pool_simple::~mem_pool_simple ()
{
  for (size_t i = 0; i < d_blocks_num; i++) {
    delete[] (uint8_t *) d_blocks[i].ptr;
  }
}

msg::sptr
mem_pool_simple::new_msg ()
{
  mem_block_t *b = d_free_blocks.pop (0);
  if(b) {
    return msg::make_shared (b->key, b->ptr, d_block_size, shared_from_this ());
  }
  return nullptr;
}

}  // namespace core

}  // namespace eqnx
