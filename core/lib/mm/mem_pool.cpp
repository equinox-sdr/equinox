/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/mem_pool.h>

namespace eqnx
{

namespace core
{
mem_pool::mem_pool (size_t msg_num, size_t max_msg_size) :
        d_blocks_num (msg_num),
        d_block_size (max_msg_size),
        d_blocks (new mem_block_t[msg_num]),
        /*
         * NOTE:
         * The circular buffer implementation needs a power-of-2 to work
         * efficiently. This is not a problem as if the parameter given here
         * is not a power-of-2, it will be scaled up accordingly. However,
         * the actual number of the circular buffer elements that can be used
         * is one less.
         */
        d_free_blocks (1, msg_num + 1)
{
  /* Initially all blocks are free */
  for (size_t i = 0; i < msg_num; i++) {
    d_blocks[i].key = i;
    d_free_blocks.push (&d_blocks[i]);
  }
}

mem_pool::~mem_pool ()
{
  delete[] d_blocks;
}

void
mem_pool::release (size_t key)
{
  d_free_blocks.push (&d_blocks[key]);
}

bool
mem_pool::full ()
{
  return d_free_blocks.empty ();
}

}  // namespace core

}  // namespace eqnx

