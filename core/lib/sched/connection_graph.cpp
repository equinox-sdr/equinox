/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/connection_graph.h>
#include <algorithm>
#include <equinox/runtime/exception.h>

namespace eqnx
{

namespace core
{

/**
 * Compares two connected components depending on their size
 * @param c0 the first connected component
 * @param c1 the second connected component
 * @return true if the first is smaller than the second, false otherwise
 */
static inline bool
connected_component_size_comparator (connected_component::sptr & c0,
                                     connected_component::sptr & c1)
{
  if (c0->size () < c1->size ()) {
    return true;
  }
  return false;
}

connection_graph::sptr
connection_graph::make_shared ()
{
  return std::shared_ptr<connection_graph> (new connection_graph ());
}

/**
 *
 * @return true if the graph is analyzed by a call of \a
 */
bool
connection_graph::is_analyzed ()
{
  return d_analyzed;
}

connection_graph::connection_graph () :
        d_analyzed (false),
        d_edge_num (0)
{
}

/**
 *
 * @return the number of nodes in the graph
 */
size_t
connection_graph::size ()
{
  return d_nodes.size ();
}

/**
 * Add a  new connection on the graph. The method will automatically
 * add to the graph the kernels participating in the connection.
 * @param c a connection between two kernels
 */
void
connection_graph::add_connection (connection::sptr c)
{
  if (d_nodes.count (c->src_kernel ()) == 0) {
    d_nodes[c->src_kernel ()] = connection_graph_node::make_shared ();
  }

  if (d_nodes.count (c->dst_kernel ()) == 0) {
    d_nodes[c->dst_kernel ()] = connection_graph_node::make_shared ();
  }

  d_nodes[c->src_kernel ()]->set_kernel (c->src_kernel ());
  d_nodes[c->src_kernel ()]->add_output_connection (c);
  d_nodes[c->dst_kernel ()]->set_kernel (c->dst_kernel ());
  d_nodes[c->dst_kernel ()]->add_input_connection (c);
  d_edge_num++;

  /* Any change on the graph invalidates the last analysis */
  d_analyzed = false;
}

/**
 * Analyzes the connection graph. This analysis includes the process of
 * listing the source and sink kernels.
 *
 * If the graph is already analyzed by a previous call of this method,
 * the previous analysis is invalidated.
 */
void
connection_graph::analyze ()
{
  std::deque<connection_graph_node> stack;
  /*
   * If for some reason the graph has been already analyzed, invalidate
   * the old analysis and do it again
   */
  invalidate ();

  /*
   * Find sources and sinks kernels. These type of kernels are quite
   * important for the workload distribution that will be performed
   * by the load balancer
   */
  for (std::pair<kernel::sptr, connection_graph_node::sptr> i : d_nodes) {
    if (i.first->input_num () == 0) {
      d_sources.push_back (i.first);
    }

    if (i.first->output_num () == 0) {
      d_sinks.push_back (i.first);
    }
  }

  /*
   * If no sources or sinks found, this means that the graph contains
   * cycles
   */
  if (d_sources.size () == 0 || d_sinks.size () == 0) {
    throw exception (exception::INVALID_GRAPH);
  }

  /*
   * Find the connected components of the graph and topologically sort
   * each one of them
   */
  find_connected_components ();

  /*
   * It is very convenient to sort the connected components based
   * on their size. So the first connected component will be the smallest
   * one and the last the larger.
   */
  std::sort (d_connected_components.begin (), d_connected_components.end (),
             connected_component_size_comparator);

  d_analyzed = true;
}

std::ostream&
operator<< (std::ostream& out, const connection_graph& g)
{
  out << "Connection graph with " << g.d_nodes.size () << " nodes and "
      << g.d_edge_num << " edges" << std::endl;
  out << "Analyzed : ";
  if (g.d_analyzed) {
    out << "Yes" << std::endl;
  }
  else {
    out << "No" << std::endl;
  }

  out << "Sources  : ";
  if (g.d_sources.size () == 0) {
    out << "N/A";
  }
  else {
    for (kernel::sptr i : g.d_sources) {
      out << std::endl;
      out << "----> " << (*i);
    }
  }
  out << std::endl;

  out << "Sinks    :";
  if (g.d_sinks.size () == 0) {
    out << "N/A";
  }
  else {
    for (kernel::sptr i : g.d_sinks) {
      out << std::endl;
      out << "----> " << (*i);
    }
  }
  return out;
}

/**
 * Return the node of the graph that corresponds to the kernel @p k
 * @param k the kernel shared pointer
 * @return the node of the graph that corresponds to the kernel @p k. if the
 * specified kernel does not have a node on the graph an
 * \ref std::out_of_range exception is raised
 */
connection_graph_node::sptr
connection_graph::operator[] (kernel::sptr k)
{
  return d_nodes.at (k);
}

/**
 * Invalidate any graph analysis performed
 */
void
connection_graph::invalidate ()
{
  d_analyzed = false;
  d_sinks.clear ();
  d_sources.clear ();

  /* Reset all graph analysis parameters set on the nodes */
  for (std::pair<kernel::sptr, connection_graph_node::sptr> i : d_nodes) {
    i.second->reset ();
  }

}

/**
 *
 * @return the number of the connected components of the graph
 */
size_t
connection_graph::get_connected_components_num ()
{
  return d_connected_components.size ();
}

/**
 * Get the kernels in a list form of a connected component. The indexing
 * of each connected component is based on an ascending order based on their
 * size. Connected component 0 is the smallest, whereas the last is the
 * larger.
 *
 * @param index the index of the connected component
 * @return a std::deque<kernel::sptr> with the kernels of the connected
 * component in topological order.
 */
const std::deque<kernel::sptr>&
connection_graph::get_connected_component_kernels (size_t index)
{
  return d_connected_components[index]->get_kernels ();
}

/**
 * Get the size of the connected component. The indexing
 * of each connected component is based on an ascending order based on their
 * size. Connected component 0 is the smallest, whereas the last is the
 * larger.
 *
 * @param index the index of the connected component
 * @return the number of kernels that belong to the connected component
 */
size_t
connection_graph::get_connected_component_size (size_t index)
{
  return d_connected_components[index]->size ();
}

/**
 * Simple DFS traversal of the graph
 * @param node the node to visit
 */
void
connection_graph::dfs (kernel::sptr node)
{
  size_t i;
  size_t n_comp;
  kernel::sptr k;
  connection_graph_node::sptr v = d_nodes[node];
  v->set_dfs_visited (true);

  /* Add the node to the last created connected component */
  d_connected_components.back ()->add_kernel (v->get_kernel (), v);

  /*
   * We need to emulate a undirected graph so visit both
   * incoming and outgoing connections
   */
  for (i = 0; i < v->get_input_edges_num (); i++) {
    k = v->get_input_edge (i)->src_kernel ();
    if (!d_nodes[k]->dfs_visited ()) {
      dfs (k);
    }
  }
  for (i = 0; i < v->get_output_edges_num (); i++) {
    k = v->get_output_edge (i)->dst_kernel ();
    if (!d_nodes[k]->dfs_visited ()) {
      dfs (k);
    }
  }
}

/**
 * Analyzes the graph and extracts the connected components.
 *
 * Because each connected component does not depend on any other
 * node of another component, the scheduler should assign each of the
 * connected components into different workers.
 */
void
connection_graph::find_connected_components ()
{
  /* Reset the DFS visited information for every node in the graph */
  for (std::pair<kernel::sptr, connection_graph_node::sptr> m : d_nodes) {
    m.second->set_dfs_visited (false);
  }

  for (std::pair<kernel::sptr, connection_graph_node::sptr> m : d_nodes) {
    if (!m.second->dfs_visited ()) {
      /* Create a new connected component */
      d_connected_components.push_back (connected_component::make_shared ());
      dfs (m.second->get_kernel ());
    }
  }

  /* Topologically sort each one */
  for (connected_component::sptr i : d_connected_components) {
    i->analyze ();
  }
}

}  // namespace core

}  // namespace eqnx

