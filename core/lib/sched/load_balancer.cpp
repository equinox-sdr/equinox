/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/load_balancer.h>
#include <equinox/runtime/exception.h>

namespace eqnx
{

namespace core
{
/**
 * Construct the load balancing subsystem
 *
 * @param graph the graph representation of the sketch
 *
 * @param workers_num the number of workers. Note that this is just
 * a hint and not a strict number. The actual number of the required
 * workers may be greater after the graph analysis.
 */
load_balancer::load_balancer (connection_graph::sptr graph, size_t workers_num) :
        d_graph (graph),
        d_n_workers (workers_num),
        d_actual_workers (0),
        d_graph_analyzed (false)
{
  if (workers_num < 1) {
    throw std::invalid_argument ("Workers number should be > 0");
  }

  if (!graph) {
    throw std::invalid_argument ("Invalid connection graph");
  }
}

load_balancer::~load_balancer ()
{
}

/**
 *
 * @return the actual number of the required workers. This may be larger
 * than the number passed as hint during the contruction of the load
 * balancer, in cases where the number of the connected components of the
 * graph is larger than the number of workers suggested. It may be also less,
 * in cases where the number of the available kernels in a graph is less
 * than the number of the suggested workers.
 */
size_t
load_balancer::actual_workers_num ()
{
  if (!d_graph_analyzed) {
    throw exception (exception::LOAD_BALANCER_ANALYSIS_NOT_PERFORMED);
  }
  if (!d_actual_workers) {
    throw exception (exception::LOAD_BALANCER_INVALID_ANALYSIS);
  }
  return d_actual_workers;
}
}  // namespace core

}  // namespace eqnx

