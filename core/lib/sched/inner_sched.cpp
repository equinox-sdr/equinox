/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/inner_sched.h>

namespace eqnx
{

namespace core
{

inner_sched::inner_sched (const std::string& name) :
        d_name (name),
        d_n_kernels (0),
        d_is_ready (false),
        d_running (false),
        d_start (false),
        d_stop (false)
{
}

inner_sched::~inner_sched ()
{
}

std::string
inner_sched::name ()
{
  return d_name;
}

bool
inner_sched::is_ready ()
{
  return d_is_ready;
}

void
inner_sched::set_ready ()
{
  d_is_ready = true;
}

void
inner_sched::stop ()
{
  d_stop = true;
}

/**
 *
 * @return true if the scheduler executes the main loop, false otherwise
 */
bool
inner_sched::is_running ()
{
  return d_running;
}

std::ostream&
operator<< (std::ostream& out, const inner_sched& y)
{
  return out << "Inner Scheduler " << y.d_name << std::endl;
}

}  // namespace core

}  // namespace eqnx
