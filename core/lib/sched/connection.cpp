/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/connection.h>
#include <equinox/runtime/exception.h>

namespace eqnx
{

namespace core
{

/**
 * Creates a shared pointer of a new connection
 * @param src the source connection anchor (kernel + port)
 * @param dst the destination connection anchor (kernel + port)
 * @return a connection shared pointer
 */
connection::sptr
connection::make_shared (const connection_anchor& src,
                         const connection_anchor& dst)
{
  return std::shared_ptr<connection> (new connection (src, dst));
}

connection::connection (const connection_anchor& src,
                        const connection_anchor& dst) :
        d_src (src.get_kernel ()),
        d_dst (dst.get_kernel ()),
        d_weight (1.0f),
        d_processed (false)
{
  /* Check if the source and destination ports are output and input */
  if (!src.get_port ()->is_output ()) {
    throw exception (exception::SRC_PORT_IS_NOT_OUTPUT);
  }

  if (!dst.get_port ()->is_input ()) {
    throw exception (exception::DST_PORT_IS_NOT_INPUT);
  }

  /*
   * Down cast the ports from their generic IO object back to their
   * proper type. Normally this always succeeds because the automatic
   * code generation tool takes care of it.
   *
   * However, proper checks are performed to ensure the correctness even
   * if a user create a by-the-hand connection.
   * While the connection checks etc are performed before the actual
   * execution phase, we use dynamic_pointer_cast<> to ensure proper
   * downcasting, without worrying about the penalty that this type of cast
   * introduces.
   */
  d_src_port = std::dynamic_pointer_cast<output_port> (src.get_port ());
  if (!d_src_port) {
    throw exception (exception::SRC_PORT_DOWNCAST_ERROR);
  }
  d_dst_port = std::dynamic_pointer_cast<input_port> (dst.get_port ());
  if (!d_dst_port) {
    throw exception (exception::DST_PORT_DOWNCAST_ERROR);
  }
}

/**
 *
 * @return the source kernel of this connection
 */
kernel::sptr
connection::src_kernel ()
{
  return d_src;
}

/**
 * @return the destination kernel of this connection
 */
kernel::sptr
connection::dst_kernel ()
{
  return d_dst;
}

/**
 *
 * @return the source (output) port of this connection
 */
output_port::sptr
connection::src_port ()
{
  return d_src_port;
}

/**
 *
 * @return the destination (input) port of this connection
 */
input_port::sptr
connection::dst_port ()
{
  return d_dst_port;
}

/**
 *
 * @return the weight of the connection
 */
float
connection::weight ()
{
  return d_weight;
}

void
connection::set_weight (float w)
{
  if (w > 1.0f) {
    throw std::runtime_error ("Invalid weight on connection");
  }
  if (w > 0.0) {
    d_weight = w;
    return;
  }
  throw std::runtime_error ("Invalid weight on connection");
}

/**
 *
 * @return true if the connections has been processed, false otherwise
 */
bool
connection::is_processed ()
{
  return d_processed;
}

/**
 * Sets the processing status of the connection
 * @param processed true if the connections has been processed, false otherwise
 */
void
connection::set_processed (bool processed)
{
  d_processed = processed;
}

}  // namespace core

}  // namespace eqnx
