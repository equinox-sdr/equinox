/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/load_balancer_simple.h>
#include <equinox/runtime/exception.h>

#include <cmath>
#include <algorithm>

namespace eqnx
{

namespace core
{
load_balancer_simple::load_balancer_simple (connection_graph::sptr graph,
                                            size_t workers_num) :
        load_balancer (graph, workers_num),
        d_split_margin (0.25)
{
}

bool
load_balancer_simple::analyze ()
{
  size_t n_components;
  /* Start with the graph analysis */
  d_graph->analyze ();
  if (!d_graph->is_analyzed ()) {
    return false;
  }

  n_components = d_graph->get_connected_components_num ();

  /*
   * Workers should not be less than the number of the connected components
   * of the graph
   */
  d_actual_workers = std::max (n_components, d_n_workers);

  /*
   * The number of workers should not be larger than the number of
   * available kernels
   */
  d_actual_workers = std::min (d_actual_workers, d_graph->size ());
  balance_connected_components ();
  d_graph_analyzed = true;
  return true;
}

void
load_balancer_simple::reset ()
{
  d_graph_analyzed = false;
  d_workers_assignement.clear ();
}

std::deque<kernel::sptr>
load_balancer_simple::get_worker_assignement (size_t index)
{
  if (!d_graph_analyzed) {
    throw exception (exception::LOAD_BALANCER_ANALYSIS_NOT_PERFORMED);
  }
  return *d_workers_assignement[index];
}

bool
load_balancer_simple::find_kernel(const size_t index, const kernel::sptr k)
{
  if (!d_graph_analyzed) {
    throw exception (exception::LOAD_BALANCER_ANALYSIS_NOT_PERFORMED);
  }
  if(index > d_workers_assignement.size() - 1) {
    return false;
  }
  if (std::find (d_workers_assignement[index]->begin (),
                 d_workers_assignement[index]->end (), k)
      == d_workers_assignement[index]->end ()) {
    return false;
  }
  return true;
}

/**
 * This method balances the kernel of a connected component into
 * a number of available workers.
 *
 * @param index the index of the connected component
 * @return the number of assigned workers
 */
size_t
load_balancer_simple::balance_connected_component (size_t index,
                                                   size_t available_workers,
                                                   size_t kernels_per_cc)
{
  size_t n_workers = 0;
  size_t assigned = 0;
  size_t n_kernels = d_graph->get_connected_component_size (index);
  size_t remaining = n_kernels;
  std::shared_ptr<std::deque<kernel::sptr>> worker_kernels (
      new std::deque<kernel::sptr> ());

  /* Just an initial coarse estimation */
  size_t est = n_kernels / kernels_per_cc;
  size_t margin = std::max (1.0f, d_split_margin * kernels_per_cc);

  const std::deque<kernel::sptr>& cc_kernels =
      d_graph->get_connected_component_kernels (index);

  /*
   * Perhaps the entire connected component can be assigned to a
   * single worker
   */
  if (est <= 1 || (est == 1 && (n_kernels % kernels_per_cc) < margin)) {
    for (kernel::sptr k : cc_kernels) {
      worker_kernels->push_back (k);
    }
    d_workers_assignement.push_back (worker_kernels);
    return 1;
  }

  auto iter = cc_kernels.begin ();
  while (remaining) {
    worker_kernels->push_back (*iter);
    iter++;
    remaining--;
    assigned++;

    if (assigned >= kernels_per_cc && remaining > margin) {
      d_workers_assignement.push_back (worker_kernels);
      assigned = 0;
      n_workers++;

      /* Create a new one */
      worker_kernels = std::shared_ptr<std::deque<kernel::sptr>> (
          new std::deque<kernel::sptr> ());

      /* Check if we reached the number of available workers */
      if (n_workers == available_workers) {
        break;
      }
    }
  }

  /* Perhaps due to the margin, the last worker is not yet committed */
  if (worker_kernels->size () != 0) {
    d_workers_assignement.push_back (worker_kernels);
    n_workers++;
  }

  /* Assign the left over kernels to the last worker */
  while (remaining) {
    d_workers_assignement[d_workers_assignement.size () - 1]->push_back (*iter);
    iter++;
  }
  return n_workers;
}

void
load_balancer_simple::balance_connected_components ()
{
  size_t available_workers = d_actual_workers;
  size_t n_cc = d_graph->get_connected_components_num ();
  size_t n_kernels = d_graph->size ();

  size_t kernels_per_cc = std::ceil ((float) n_kernels / available_workers);

  /*
   * Start from the smallest connected component and try to apply evenly
   * the kernels on the available workers
   */
  for (size_t i = 0; i < n_cc; i++) {
    size_t assigned = balance_connected_component (i, available_workers,
                                                   kernels_per_cc);
    available_workers -= assigned;
  }

  /*
   * Now the actual number of workers becomes the size of the vector
   * with the connected components
   */
  d_actual_workers = d_workers_assignement.size ();
}

}  // namespace core

}  // namespace eqnx

