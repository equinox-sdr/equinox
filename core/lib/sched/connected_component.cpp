/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017,2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/connected_component.h>

#include <equinox/runtime/exception.h>

namespace eqnx
{

namespace core
{

/**
 * Make a shared pointer reference to a connected component object
 * @return shared pointer of the connected component object
 */
connected_component::sptr
connected_component::make_shared ()
{
  return std::shared_ptr<connected_component> (new connected_component ());
}

/**
 * Add a new kernel that is part of the connected component.
 * Any previous analysis on the connected component graph is
 * invalidated.
 *
 * @param k the kernel shared pointer
 * @param knode the graph node shared pointer associated with the kernel k
 */
void
connected_component::add_kernel (kernel::sptr k,
                                 connection_graph_node::sptr knode)
{
  /* Invalidate any analysis performed */
  d_analyzed = false;

  if (d_nodes_map.count (k) != 0) {
    throw exception (exception::INVALID_GRAPH_OP);
  }

  d_nodes_map[k] = knode;

  if (k->is_source ()) {
    d_sources.push_back (k);
  }

  if (k->is_sink ()) {
    d_sinks.push_back (k);
  }
}

/**
 * Perform graph analysis
 */
void
connected_component::analyze ()
{
  /* At least one source and one sink kernel should exist */
  if (d_sources.size () == 0 || d_sinks.size () == 0) {
    throw exception (exception::INVALID_GRAPH);
  }

  topological_sort ();
  d_analyzed = true;
}

/**
 *
 * @return true if the graph analysis of the connected component has been
 * successfully performed or false otherwise
 */
bool
connected_component::is_analyzed ()
{
  return d_analyzed;
}

/**
 * Get the kernels of the connected component in topological order
 * @return a std::deque<kernel::sptr>& with the list of the kernels.
 * Head points to the first kernel in topological ordering
 */
const std::deque<kernel::sptr>&
connected_component::get_kernels ()
{
  if (!d_analyzed) {
    analyze ();
  }
  return d_top_order;
}

/**
 *
 * @return the number of kernels in the connected component
 */
size_t
connected_component::size ()
{
  if (!d_analyzed) {
    analyze ();
  }
  return d_top_order.size ();
}

connected_component::connected_component () :
        d_analyzed (false)
{
}

/**
 * Topologically sorts the graph using the Kahl's algorithm. This method
 * creates a list of the nodes in topological order that can be used
 * to iterate among them.
 */
void
connected_component::topological_sort ()
{
  connection_graph_node::sptr n;
  connection::sptr edge;
  std::deque<kernel::sptr> s;

  /* Source kernels are important. Start from them */
  for (kernel::sptr i : d_sources) {
    s.push_back (i);
  }

  /*
   * Invalidate previous topological order
   */
  d_top_order.clear ();

  while (!s.empty ()) {
    n = d_nodes_map[s.front ()];
    s.pop_front ();
    d_top_order.push_back (n->get_kernel ());

    for (std::pair<kernel::sptr, connection_graph_node::sptr> m : d_nodes_map) {
      /* Get the incoming edges from n to m that have not yet processed */
      for (size_t i = 0; i < m.second->get_input_edges_num (); i++) {
        if (m.second->get_input_edge (i)->src_kernel ()
            == n->get_kernel ()
            && !m.second->get_input_edge (i)->is_processed ()) {
          m.second->get_input_edge (i)->set_processed (true);
          if (m.second->get_input_edges_num (false) == 0) {
            s.push_back (m.first);
          }
        }
      }
    }

    /*
     * Sanity check. The non-cycles property should be ensured by upper
     * layers, but an extra security check is not in vain
     */
    if (d_top_order.size () > d_nodes_map.size ()) {
      throw exception (exception::INVALID_GRAPH);
    }
  }
}

}  // namespace core

}  // namespace eqnx

