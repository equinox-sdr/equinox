/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/load_balancer_spectral.h>
#include <equinox/runtime/exception.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>

#include <limits>

namespace eqnx
{

namespace core
{

/**
 * Load balancer that performs graph partitioning using
 * the spectral method, based on the Fielder's eigenvalues and eigenvectors
 * theory.
 *
 * @param graph the input graph
 * @param workers_num how many partitions to create
 * @param min_partition_size the minimum number of kernels in a partition
 */
load_balancer_spectral::load_balancer_spectral (connection_graph::sptr graph,
                                                size_t workers_num,
                                                size_t min_partition_size) :
        load_balancer (graph, workers_num),
        d_min_partition_size (min_partition_size)
{
}

/**
 * Performs the graph analysis and creates the partitions
 * @return true on success or false if the partitions could not be created
 */
bool
load_balancer_spectral::analyze ()
{
  size_t n_components;
  partition_result_t partitions;

  /* Start with the graph analysis */
  d_graph->analyze ();
  if (!d_graph->is_analyzed ()) {
    return false;
  }

  n_components = d_graph->get_connected_components_num ();
  if (n_components > d_n_workers) {
    throw exception (exception::LOAD_BALANCER_TOO_FEW_WORKERS);
  }

  /* Perform the load balancing on all the connected components */
  balance_connected_components ();
  if (d_actual_workers) {
    d_graph_analyzed = true;
    return true;
  }
  return false;
}

/**
 * Resets the current graph analysis and the existing partitions
 */
void
load_balancer_spectral::reset ()
{
  d_graph_analyzed = false;
  d_workers_assignement.clear ();
}


std::deque<kernel::sptr>
load_balancer_spectral::get_worker_assignement (size_t index)
{
  if (!d_graph_analyzed) {
    throw exception (exception::LOAD_BALANCER_ANALYSIS_NOT_PERFORMED);
  }
  return *d_workers_assignement[index];
}

bool
load_balancer_spectral::find_kernel(const size_t index, const kernel::sptr k)
{
  if (!d_graph_analyzed) {
    throw exception (exception::LOAD_BALANCER_ANALYSIS_NOT_PERFORMED);
  }
  if(index > d_workers_assignement.size() - 1) {
    return false;
  }
  if (std::find (d_workers_assignement[index]->begin (),
                 d_workers_assignement[index]->end (), k)
      == d_workers_assignement[index]->end ()) {
    return false;
  }
  return true;
}

void
load_balancer_spectral::balance_connected_components ()
{
  size_t n_components = d_graph->get_connected_components_num ();
  /* A coarse estimation of the number of kernels per partition */
  size_t kernels_per_partition = std::max (1UL, d_graph->size () / d_n_workers);

  /* Split the graph of each connected component independently */
  for (size_t i = 0; i < n_components; i++) {
    /*
     * Get the kernels of the connected components and assign them in
     * a new deque for easier internal handling
     */
    const std::deque<kernel::sptr>& cc =
        d_graph->get_connected_component_kernels (i);

    std::shared_ptr<std::deque<kernel::sptr>> dq (
        new std::deque<kernel::sptr> ());
    for (kernel::sptr i : cc) {
      dq->push_back (i);
    }

    size_t k = cc.size () / kernels_per_partition;

    spectral_graph_partition_recursive (dq, k);
  }
  d_actual_workers = d_workers_assignement.size ();
}

/**
 * Find the second smallest eigenvalue
 * @param eigenvals the eigenvalues of the Laplacian
 * @return
 */
static size_t
find_l2_idx (
    const Eigen::EigenSolver<Eigen::MatrixXf>::EigenvalueType& eigenvals)
{
  size_t i;
  size_t min_idx = 0;
  size_t min2_idx = 0;
  float min_val = std::real (eigenvals[0]);
  float min2_val = std::numeric_limits<float>::max ();
  /* Find first the minimum */
  for (i = 1; i < eigenvals.size (); i++) {
    if (std::real (eigenvals[i]) < min_val) {
      min_idx = i;
      min_val = std::abs (eigenvals[i]);
    }
  }

  for (i = 0; i < eigenvals.size (); i++) {
    if (std::real (eigenvals[i]) < min2_val && i != min_idx) {
      min2_idx = i;
      min2_val = std::real (eigenvals[i]);
    }
  }
  return min2_idx;
}

void
load_balancer_spectral::spectral_graph_partition_recursive (
    const std::shared_ptr<std::deque<kernel::sptr> > g, size_t k)
{
  /* Partitioning is not possible */
  if (k < 2) {
    d_workers_assignement.push_back (g);
    return;
  }

  partition_result_t res = spectral_graph_partition (g);
  /*
   * Result may be 1 or 2 sub-graphs. One subgraph can be obtained only
   * when the subgraph size limit is reached. Therefore, it is also the
   * recursion stop signal
   */
  if (res.size () < 2) {
    d_workers_assignement.push_back (res[0]);
    return;
  }
  else {
    spectral_graph_partition_recursive (res[0], k / 2);
    spectral_graph_partition_recursive (res[1], k / 2);
  }
}

/**
 * Performs the Fielder's eigenvalue analysis on the graph @e g
 * @param g the graph to partition
 * @return a vector with at least one and at most two subgraphs consisting
 * the partitioning
 */
load_balancer_spectral::partition_result_t
load_balancer_spectral::spectral_graph_partition (
    const std::shared_ptr<std::deque<kernel::sptr> > g)
{

  /*
   * If the sub-graph contains a number of kernels less than the
   * minimum allowed return as result the input graph itself
   */
  if (g->size () <= d_min_partition_size) {
    return {g};
  }

  /* Create the adjacency matrix and the degree matrix */
  size_t n_vertices = g->size ();
  Eigen::MatrixXf adj_mtx = Eigen::MatrixXf::Zero (n_vertices, n_vertices);
  Eigen::MatrixXf deg_mtx = Eigen::MatrixXf::Zero (n_vertices, n_vertices);
  Eigen::MatrixXf laplacian;

  for (size_t i = 0; i < n_vertices; i++) {
    for (size_t j = 0; j < n_vertices; j++) {
      connection_graph_node::sptr s = (*d_graph)[(*g)[i]];
      connection_graph_node::sptr d = (*d_graph)[(*g)[j]];
      size_t n_in_edges = s->get_input_edges_num ();
      size_t n_out_edges = s->get_output_edges_num ();

      for (size_t edge = 0; edge < n_in_edges; edge++) {
        if (s->get_input_edge (edge)->src_kernel () == (*g)[j]) {
          adj_mtx (i, j) = 1;
        }
      }

      for (size_t edge = 0; edge < n_out_edges; edge++) {
        if (s->get_output_edge (edge)->dst_kernel () == (*g)[j]) {
          adj_mtx (i, j) = 1;
        }
      }

      if (i == j) {
        deg_mtx (i, j) = n_in_edges + n_out_edges;
      }
    }
  }

  /* Create the Laplacian matrix */
  laplacian = deg_mtx - adj_mtx;

  /* Compute eigenvalues and eigenvectors */
  Eigen::EigenSolver<Eigen::MatrixXf> es (laplacian);

  /* Find the second smallest eigenvalue index */
  size_t l2_idx = find_l2_idx (es.eigenvalues ());
  /*
   * Make the partition based on the values of the eignevector that
   * corresponds ro l2
   */
  std::shared_ptr<std::deque<kernel::sptr>> g0 (
      new std::deque<kernel::sptr> ());
  std::shared_ptr<std::deque<kernel::sptr>> g1 (
      new std::deque<kernel::sptr> ());

  for (size_t i = 0; i < n_vertices; i++) {
    if (std::real (es.eigenvectors ().col (l2_idx)[i]) < 0) {
      g0->push_back ((*g)[i]);
    }
    else {
      g1->push_back ((*g)[i]);
    }
  }
  return {g0, g1};
}

}  // namespace core

}  // namespace eqnx

