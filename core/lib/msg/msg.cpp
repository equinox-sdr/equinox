/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/msg/msg.h>
#include <equinox/core/mm/mem_pool.h>

namespace eqnx
{

std::shared_ptr<eqnx::msg>
eqnx::msg::make_shared (size_t key, void* const mem, size_t size,
                        std::shared_ptr<core::mem_pool> pool)
{
  return std::shared_ptr<msg> (new msg (key, mem, size, pool));
}

msg::msg (size_t key, void* const mem, size_t size,
          std::shared_ptr<core::mem_pool> pool) :
        d_key (key),
        d_max_size (size),
        d_mem_pool (pool),
        d_ptr (mem),
        d_nsamples (0)
{
  if (!d_mem_pool) {
    throw std::invalid_argument ("Invalid memory pool");
  }
}

msg::~msg ()
{
  d_mem_pool->release (d_key);
}

/**
 *
 * @return the identifier of the message
 */
size_t
msg::key ()
{
  return d_key;
}

/**
 * Returns the raw pointer of the message
 */
void *
msg::raw_ptr ()
{
  return d_ptr;
}

}  // namespace eqnx
