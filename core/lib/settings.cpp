/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/settings.h>

namespace eqnx
{
namespace core
{

/**
 * This option specifies the maximum number of workers per CPU core.
 * If set to zero, the scheduler may assign an arbitrary number of
 * workers in each core.
 *
 * Default: 1
 */
const std::string settings::workers_per_core_attr = "workers_per_core";

/**
 * This setting specifies the maximum size of a message.
 * It can be overwritten by any kernel.
 *
 * Default: 1024
 */
const std::string settings::max_msg_size_attr = "max_msg_size";

/**
 * Creates the main Equinox configuration object based on the settings
 * stored in a file
 * @param file the file path containing the configuration parameters
 */
settings::settings (const std::string& file) :
        d_file (file),
        d_workers_per_core (default_workers_per_core),
        d_max_msg_size (default_max_msg_size)
{
  libconfig::Config conf;
  conf.readString (file);
  const libconfig::Setting& root = conf.getRoot ();

  try {
        d_workers_per_core = root[workers_per_core_attr.c_str()];
  }
  catch (const libconfig::SettingNotFoundException &e) {
    d_workers_per_core = default_workers_per_core;
  }
  try {
        d_workers_per_core = root[max_msg_size_attr.c_str()];
  }
  catch (const libconfig::SettingNotFoundException &e) {
    d_max_msg_size = default_max_msg_size;
  }
}

std::shared_ptr<settings>
settings::make ()
{
  return std::shared_ptr<settings> (new settings ());
}

std::shared_ptr<settings>
settings::make (const std::string& file)
{
  return std::shared_ptr<settings> (new settings (file));
}

size_t
settings::get_workers_per_core ()
{
  return d_workers_per_core;
}

size_t
settings::get_max_msg_size ()
{
  return d_max_msg_size;
}

void
settings::set_workers_per_core (size_t n)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  d_workers_per_core = n;
}

void
settings::set_max_msg_size (size_t s)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  d_max_msg_size = s;
}

std::string
settings::config_file ()
{
  return d_file;
}

void
settings::store ()
{
  if (d_file.length () == 0) {
    return;
  }
  store (d_file);
}

void
settings::store (const std::string& file)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  libconfig::Config conf;
  libconfig::Setting& root = conf.getRoot ();
  root.add (workers_per_core_attr, libconfig::Setting::TypeInt) =
      (int) d_workers_per_core;
  root.add (max_msg_size_attr, libconfig::Setting::TypeInt) =
      (int) d_max_msg_size;
  conf.writeFile (file.c_str ());
}

/**
 * Creates the main Equinox configuration object based on the default
 * settings
 */
settings::settings () :
        d_file (""),
        d_workers_per_core (default_workers_per_core),
        d_max_msg_size (default_max_msg_size)
{
}

settings::~settings ()
{
}

} /* namespace core */
} /* namespace eqnx */
