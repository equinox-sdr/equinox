# Equinox: SDR Platform for realtime applications
Equinox is a C++11 based SDR platform targeting applications where realtime
execution and resources utilization is the primary concern.

## Testing
Equinox supports extensive Unit testing using the 
[CPPUnit](http://cppunit.sourceforge.net/) library.
Unit tests are enabled by default if the 
[CPPUnit](http://cppunit.sourceforge.net/) library
is installed on the build platform. 
However they can be easily disabled setting properly the CMake variable 
`ENABLE_TESTING`. Eg:

    cmake -DENABLE_TESTING=OFF

In addition, each Unit test is checked for memory leaks using the 
[Valgrind](http://valgrind.org) memory check tool.
Again memory checks can be overridden using the CMake variable `ENABLE_MEMCHECK`.


After the compilation process, Unit tests can be executed using the command 

    make test

### Benchmark
The Equinox platform integrates a set of benchmarks on the core modules of the
platform that will help to identify the best settings, in terms of performance,
for the host platform. These micro-benchmarks are using the 
[Google Benchmark](https://github.com/google/benchmark) suite. Benchmarks tests
are enabled by default if the corresponding library is installed on the build
platform, otherwise they are automatically disabled. They can also explicitly
be disabled using the `ENABLE_BENCHMARK` CMake option. Benchmarks are part
of the CMake testing suite and are executed among the other tests using the command

    make test

## Build system Variables summary
The list below provides all the build system options that are available:

* `ENABLE_TESTING`: Enable/Disable Unit testing
* `ENABLE_MEMCHECK`: Enable/Disable memory checking on the Unit tests
* `ENABLE_BENCHMARK`: Enable/Disable performance benchmarks
