#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###############################################################################
# CMake module for the Eigen3 library
# This module defines the variables:
#    EIGEN3_FOUND: True if the library found, undefined otherwise
#    EIGEN3_INCLUDE_DIR: The include directory
###############################################################################

INCLUDE(FindPkgConfig)

PKG_CHECK_MODULES(PKG_EIGEN3 eigen3)

if (PKG_EIGEN3_FOUND)
        FIND_PATH(EIGEN3_INCLUDE_DIR
        NAMES Eigen/Eigen
        HINTS ${PKG_EIGEN3_INCLUDE_DIRS}
        PATHS /usr/include/
              /usr/local/include)
else (PKG_EIGEN3_FOUND)
    FIND_PATH(EIGEN3_INCLUDE_DIR
        NAMES Eigen/Eigen
        PATHS /usr/include/Eigen3
              /usr/include/eigen3
              /usr/local/include/Eigen3
              /usr/local/include/eigen3)
endif (PKG_EIGEN3_FOUND)



if (EIGEN3_INCLUDE_DIR)
    set(EIGEN3_FOUND TRUE)
endif (EIGEN3_INCLUDE_DIR)

if (EIGEN3_FOUND)
    if (NOT Eigen3_FIND_QUIETLY)
        message(STATUS "Found Eigen3: ${EIGEN3_INCLUDE_DIR}")
    endif (NOT Eigen3_FIND_QUIETLY)
else (EIGEN3_FOUND)
    if (Eigen3_FIND_REQUIRED)
        message (FATAL_ERROR "Could not find Eigen3")
    endif (Eigen3_FIND_REQUIRED)
endif (EIGEN3_FOUND)
