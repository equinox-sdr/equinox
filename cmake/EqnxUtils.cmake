#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if(DEFINED __INCLUDED_EQNX_UTILS_CMAKE)
    return()
endif()
set(__INCLUDED_EQNX_UTILS_CMAKE TRUE)

###############################################################################
# Appends a list with files to a variable declared on a parent scope
###############################################################################
function(eqnx_append_files var filelist)
    set(tmp ${${var}})
    foreach(i ${filelist})
        list(APPEND tmp ${CMAKE_CURRENT_SOURCE_DIR}/${i})
    endforeach()
    set(${var} ${tmp} PARENT_SCOPE)
endfunction()

###############################################################################
# Propagates towards the parent directory a specific variable.
# Usefull in cases where subdirectories add source files to the compilation
# unit.
###############################################################################
macro(EQNX_PROPAGATE_VAR var)
    set(${var} ${${var}} PARENT_SCOPE)
endmacro(EQNX_PROPAGATE_VAR var)

###############################################################################
# Handy macro to set easily a vatriable with a list of include directories
###############################################################################
macro(EQNX_SET_INCLUDE_DIRS var)
  set(${var} ${ARGN} CACHE INTERNAL "" FORCE)
endmacro(EQNX_SET_INCLUDE_DIRS)

