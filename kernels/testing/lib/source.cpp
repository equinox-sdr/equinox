/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/kernels/testing/source.h>

#include <cstring>

namespace eqnx
{

namespace testing
{

source::sptr
source::make_shared (const std::string& name, size_t nports)
{
  return std::shared_ptr<source> (new source (name, nports));
}

source::source (const std::string& name, size_t nports) :
        kernel (name),
        d_cnt(0)
{
  for (size_t i = 0; i < nports; i++) {
    const std::string pname ("out" + std::to_string (i));
    new_output_port (pname, 512, 1024);
  }
}

void
source::exec ()
{
  for (size_t i = 0; i < output_num (); i++) {
    const std::string pname ("out" + std::to_string (i));
    msg::sptr m = new_msg(pname);
    memcpy (m->raw_ptr (), &d_cnt, sizeof(size_t));
    write(pname, m);
  }
  d_cnt++;
}

}  // namespace testing

}  // namespace eqnx
