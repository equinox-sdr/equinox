/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KERNELS_TESTING_INCLUDE_EQUINOX_KERNELS_TESTING_IN_OUT_H_
#define KERNELS_TESTING_INCLUDE_EQUINOX_KERNELS_TESTING_IN_OUT_H_

#include <equinox/runtime/kernel.h>

namespace eqnx
{

namespace testing
{
/**
 * @brief A testing input-output kernel with a variable number of
 * input and output ports
 */
class in_out : public virtual kernel
{
public:
  typedef std::shared_ptr<in_out> sptr;

  static sptr
  make_shared (const std::string& name, size_t n_in_ports = 1,
               size_t n_out_ports = 1);

  in_out (const std::string& name, size_t n_in_ports, size_t n_out_ports);

  void
  exec ();
};
}  // namespace testing

}  // namespace eqnx

#endif /* KERNELS_TESTING_INCLUDE_EQUINOX_KERNELS_TESTING_IN_OUT_H_ */
