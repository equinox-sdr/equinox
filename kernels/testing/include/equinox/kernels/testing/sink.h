/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KERNELS_TESTING_INCLUDE_EQUINOX_KERNELS_TESTING_SINK_H_
#define KERNELS_TESTING_INCLUDE_EQUINOX_KERNELS_TESTING_SINK_H_

#include <equinox/runtime/kernel.h>

namespace eqnx
{

namespace testing
{

/**
 * @brief A testing sink kernel with a variable number of output ports
 */
class sink : public virtual kernel
{
public:
  typedef std::shared_ptr<sink> sptr;

  static sptr
  make_shared (const std::string& name, size_t nports = 1);

  ~sink();

  void
  exec ();

protected:
  sink (const std::string& name, size_t nports);

private:
  std::vector<size_t> d_stats;
};

}  // namespace testing

}  // namespace eqnx

#endif /* KERNELS_TESTING_INCLUDE_EQUINOX_KERNELS_TESTING_SINK_H_ */
